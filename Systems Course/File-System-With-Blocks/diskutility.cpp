//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#include "diskutility.h"
#include "helper.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

bool readFromBlock(unsigned int blockIndex, char *buffer) {
    if (buffer == NULL) {
        return false;
    }

    FILE *disk = fopen(DISK, "rb");
    if (disk == NULL) {
        return false;
    }

    fseek(disk, blockIndex * BLOCK_SIZE, SEEK_SET);

    if (fread(buffer, sizeof(char), BLOCK_SIZE, disk) < 0) {
        return false;
    }
    fclose(disk);

    return true;
}

bool writeToBlock(unsigned int blockIndex, char *buffer) {
    if (buffer == NULL) {
        return false;
    }

    FILE *disk = fopen(DISK, "rb+");
    if (disk == NULL) {
        return false;
    }

    fseek(disk, blockIndex * BLOCK_SIZE, SEEK_SET);

    if (fwrite(buffer, sizeof(char), BLOCK_SIZE, disk) < 0) {
        return false;
    }
    fclose(disk);

    return true;
}

Metadata *readMetadata() {
    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    Metadata *metadata = (Metadata *)malloc(sizeof(Metadata));

    readFromBlock(0, buffer);
    memcpy(metadata, buffer, sizeof(Metadata));
    free(buffer);

    return metadata;
}

void writeMetadata(Metadata *metadata) {
    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    memcpy(buffer, metadata, sizeof(Metadata));

    writeToBlock(0, buffer);

    free(buffer);
}

void format() {
    Metadata *metadata = readMetadata();

    metadata->magicNumber = MAGIC_NUMBER;
    metadata->files = 0;
    for (int i = 0; i < BLOCKS; i++) {
        metadata->freeBlocks[i] = false;
    }

    writeMetadata(metadata);
    free(metadata);
}

void init() {
    Metadata *metadata = readMetadata();
    if (metadata->magicNumber != MAGIC_NUMBER) {
        format();
    }

    free(metadata);
}

unsigned int getFreeBlocks(int noOfBlocks) {
    Metadata *metadata = readMetadata();

    unsigned int i = METADATA_BLOCKS, j = METADATA_BLOCKS;
    while (i < BLOCKS && j < BLOCKS) {
        if (i - j == noOfBlocks) {
            return j;
        }

        if (metadata->freeBlocks[i]) {
            i++;
            j++;
        } else {
            i++;
        }
    }

    return -1;
}

void appendToMetadata(char *filename, unsigned int startBlock, unsigned int noOfBlocks, unsigned int fileLength) {
    Metadata *metadata = readMetadata();

    strcpy(metadata->records[metadata->files].filename, filename);
    metadata->records[metadata->files].startBlock = startBlock;
    metadata->records[metadata->files].noOfBlocks = noOfBlocks;
    metadata->records[metadata->files].fileLength = fileLength;
    metadata->files += 1;

    for (int i = 0; i < noOfBlocks; i++) {
        metadata->freeBlocks[startBlock + i] = true;
    }

    writeMetadata(metadata);
    free(metadata);
}

unsigned int getStartBlock(char *source) {
    Metadata *metadata = readMetadata();

    for (int i = 0; i < metadata->files; i++) {
        if (strcmp(metadata->records[i].filename, source) == 0) {
            free(metadata);
            return metadata->records[i].startBlock;
        }
    }

    free(metadata);
    return -1;
}

unsigned int getFileLength(char *source) {
    Metadata *metadata = readMetadata();

    for (int i = 0; i < metadata->files; i++) {
        if (strcmp(metadata->records[i].filename, source) == 0) {
            free(metadata);
            return metadata->records[i].fileLength;
        }
    }

    free(metadata);
    return -1;
}

void importFile(char *source, char *destination) {
    if (source == NULL || destination == NULL) {
        return;
    }

    FILE *sourceFile = fopen(source, "rb");
    if (sourceFile == NULL) {
        return;
    }
    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);;

    unsigned int sourceFileSize = getFileSize(source);
    unsigned int blocksRequired = ceil(sourceFileSize / BLOCK_SIZE);
    unsigned int block = 0;
    unsigned int remainingFileSize = sourceFileSize;
    if (blocksRequired == 0) {
        block = getFreeBlocks(1);

        blocksRequired = 1;
    }

    block = getFreeBlocks(blocksRequired);
    unsigned int blockId = block;

    for (int i = 0; i < blocksRequired; i++) {
        int copySize = 0;
        if (remainingFileSize < BLOCK_SIZE) {
            copySize = remainingFileSize;
        } else {
            copySize = BLOCK_SIZE;
        }
        if (fread(buffer, sizeof(char), copySize, sourceFile) == 0) {
            return;
        }
        if (!writeToBlock(blockId++, buffer)) {
            return;
        }
        remainingFileSize -= BLOCK_SIZE;
    }
    fclose(sourceFile);

    appendToMetadata(destination, block, blocksRequired, sourceFileSize);

    free(buffer);
}

void exportFile(char *source, char *destination) {
    if (source == NULL || destination == NULL) {
        return;
    }

    FILE *destinationFile = fopen(destination, "wb");
    if (destinationFile == NULL) {
        return;
    }

    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    unsigned int fileLength = 0;
    unsigned int noOfBlocks = ceil(fileLength / BLOCK_SIZE);
    unsigned int block = 0;

    Metadata *metadata = readMetadata();
    for (int i = 0; i < metadata->files; i++) {
        if (strcmp(metadata->records[i].filename, source) == 0) {
            block = metadata->records[i].startBlock;
            noOfBlocks = metadata->records[i].noOfBlocks;
            fileLength = metadata->records[i].fileLength;
        }
    }
    unsigned int remainingFileSize = fileLength;


    for (int i = 0; i < noOfBlocks; i++) {
        int copySize = 0;
        if (remainingFileSize < BLOCK_SIZE) {
            copySize = remainingFileSize;
        } else {
            copySize = BLOCK_SIZE;
        }

        if (!readFromBlock(block + i, buffer)) {
            return;
        }

        if (fwrite(destinationFile, copySize, 1, destinationFile) == 0) {
            return;
        }

        remainingFileSize -= BLOCK_SIZE;
    }

    free(buffer);
    fclose(destinationFile);
}

void deleteFile(char *filename) {
    Metadata *metadata = readMetadata();

    int block = -1, length = -1;
    for (int i = 0; i < metadata->files; i++) {
        if (strcmp(metadata->records[i].filename, filename) == 0) {
            block = metadata->records[i].startBlock;
            length = metadata->records[i].fileLength;
            if (block == -1 || length == -1) {
                return;
            }

            metadata->records[i] = metadata->records[metadata->files - 1];
            break;
        }
    }

    for (int i = 0; i < length; i++) {
        metadata->freeBlocks[block + i] = false;
    }
    metadata->files -= 1;

    writeMetadata(metadata);
    free(metadata);
}

void debug() {
    Metadata *metadata = readMetadata();

    printf("Magic Number: %d\n", metadata->magicNumber);
    printf("Total Files: %d\n", metadata->files);
    printf("Filename\tStart Block\tNo of Blocks\tFile Length\n");
    for (int i = 0; i < metadata->files; i++) {
        printf("%s\t%d\t%d\t%d\n", metadata->records[i].filename, metadata->records[i].startBlock, metadata->records[i].noOfBlocks, metadata->records[i].fileLength);
    }

    free(metadata);
}