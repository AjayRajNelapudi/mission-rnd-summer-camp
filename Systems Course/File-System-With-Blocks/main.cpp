#include <stdio.h>
#include "diskutility.h"
#include "helper.h"

int main() {
    init();

    while (1) {
        printf("> ");
        char *command = readCommand();

        if (stringMatchN(command, "exit", 4)) {
            break;
        } else if (stringMatchN(command, "import", 6)) {
            command = command + 7;
            char src[30], dest[30];
            sscanf(command, "%s %s", src, dest);
            importFile(src, dest);
        } else if (stringMatchN(command, "export", 6)) {
            command = command + 7;
            char src[30], dest[30];
            sscanf(command, "%s %s", src, dest);
            exportFile(src, dest);
        } else if (stringMatchN(command, "delete", 6)) {
            command += 7;
            char file[30];
            sscanf(command, "%s", file);
            deleteFile(file);
        } else if (stringMatchN(command, "format", 6)) {
            format();
        } else if (stringMatchN(command, "debug", 5)) {
            debug();
        } else {
            printf("Command not found\n");
        }
    }

    return 0;
}