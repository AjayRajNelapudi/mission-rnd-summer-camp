//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#include "helper.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

unsigned int getFileSize(char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        return -1;
    }

    fseek(file, 0, SEEK_END);
    long int size = ftell(file);
    fclose(file);

    return size;
}

void strlower(char *s) {
    int c = 0;

    while (s[c] != '\0') {
        if (s[c] >= 'A' && s[c] <= 'Z') {
            s[c] = s[c] + 32;
        }
        c++;
    }
}

bool stringMatchN(char *line, char *key, int len) {
    if (strlen(line) < len) {
        return false;
    }

    if (strlen(key) < len) {
        return false;
    }

    char commandCopy[len + 1];
    strcpy(commandCopy, line);
    commandCopy[len] = '\0';

    char keyCopy[len + 1];
    strcpy(keyCopy, key);
    keyCopy[len] = '\0';

    strlower(commandCopy);
    strlower(keyCopy);

    bool status = false;
    if (strcmp(commandCopy, keyCopy) == 0) {
        status = true;
    }
    return status;
}

char *readCommand() {
    char *command = (char *)malloc(100 * sizeof(char));
    scanf("%[^\n]s", command);
    char extra;
    scanf("%c", &extra);

    return command;
}