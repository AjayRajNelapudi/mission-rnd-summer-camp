//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#ifndef FILE_SYSTEM_BLOCKS_HELPER_H
#define FILE_SYSTEM_BLOCKS_HELPER_H

unsigned int getFileSize(char *filename);
bool stringMatchN(char *line, char *key, int len);
char *readCommand();

#endif //FILE_SYSTEM_BLOCKS_HELPER_H
