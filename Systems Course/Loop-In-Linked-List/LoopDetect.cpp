//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#include "LinkedList.h"
#include "LoopDetect.h"
#include <stddef.h>

Node *getNextOfNext(Node *head) {
    if (head == NULL) {
        return NULL;
    }

    if (head->next == NULL) {
        return NULL;
    }

    return head->next->next;
}

Node *getMeetingPoint(Node *head) {
    if (head == NULL) {
        return NULL;
    }

    Node *slowPtr = head;
    Node *fastPtr = head;

    while (slowPtr != fastPtr || slowPtr == head) {
        slowPtr = slowPtr->next;
        fastPtr = getNextOfNext(fastPtr);

        if (fastPtr == NULL) {
            return NULL;
        }
    }

    return slowPtr;
}

Node *getLoopBeginNode(Node *head) {
    if (head == NULL) {
        return NULL;
    }

    Node *meetingPoint = getMeetingPoint(head);
    if (meetingPoint == NULL) {
        return NULL;
    }

    Node *currentNode = head;

    while (currentNode != meetingPoint) {
        currentNode = currentNode->next;
        meetingPoint = meetingPoint->next;
    }

    return meetingPoint;
}