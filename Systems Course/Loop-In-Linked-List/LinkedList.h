//
// Created by Ajay Raj Nelapudi on 2019-05-25.
//

#ifndef LOOP_IN_LINKED_LIST_LINKEDLIST_H
#define LOOP_IN_LINKED_LIST_LINKEDLIST_H

typedef struct LinkedList {
    int data;
    struct LinkedList *next;
} Node;

Node *append(Node *head, int data);
void freeLinkedList(Node *head);
Node *constructLinkedListFromArray(int *elements, int n);
Node *constructLinkedListWithLoop(int *elements, int n, int loopBeginPosition);
void printList(Node *head);

#endif //LOOP_IN_LINKED_LIST_LINKEDLIST_H
