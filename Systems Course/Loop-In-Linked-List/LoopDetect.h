//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#ifndef LOOP_IN_LINKED_LIST_LOOPDETECT_H
#define LOOP_IN_LINKED_LIST_LOOPDETECT_H

Node *getMeetingPoint(Node *head);
Node *getLoopBeginNode(Node *head);

#endif //LOOP_IN_LINKED_LIST_LOOPDETECT_H
