//
// Created by Ajay Raj Nelapudi on 2019-05-25.
//

#include <stdio.h>
#include <stdlib.h>
#include "LinkedList.h"

Node *makeNode(int data) {
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;

    return newNode;
}

Node *append(Node *head, int data) {
    Node *newNode = makeNode(data);

    if (head == NULL) {
        return newNode;
    }

    Node *currentNode = head;
    while (currentNode->next != NULL) {
        currentNode = currentNode->next;
    }
    currentNode->next = newNode;

    return head;
}

void freeLinkedList(Node *head) {
    if (head == NULL) {
        return;
    }

    Node *currentNode = head;
    while (currentNode != NULL) {
        Node *temp = currentNode;
        currentNode = currentNode->next;
        free(temp);
    }
}

Node *constructLinkedListFromArray(int *elements, int n) {
    if (elements == NULL || n == 0) {
        return NULL;
    }

    Node *newHead = makeNode(elements[0]);
    Node *newList = newHead;
    
    for (int i = 1; i < n; i++) {
        newList->next = makeNode(elements[i]);
        newList = newList->next;
    }

    return newHead;
}

Node *constructLinkedListWithLoop(int *elements, int n, int loopBeginPosition) {
    if (elements == NULL || n == 0) {
        return NULL;
    }

    Node *loopBeginNode = NULL;
    Node *newHead = makeNode(elements[0]);
    Node *newList = newHead;

    for (int i = 1; i < n; i++) {
        newList->next = makeNode(elements[i]);
        newList = newList->next;

        if (i + 1 == loopBeginPosition) {
            loopBeginNode = newList;
        }
    }

    newList->next = loopBeginNode;

    return newHead;
}

void printList(Node *head) {
    while (head != NULL) {
        printf("%d->", head->data);
        head = head->next;
    }
}
