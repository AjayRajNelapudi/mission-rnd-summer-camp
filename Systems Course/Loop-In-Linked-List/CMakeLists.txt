cmake_minimum_required(VERSION 3.14)
project(Loop_In_Linked_List)

set(CMAKE_CXX_STANDARD 14)

add_executable(Loop_In_Linked_List main.cpp LinkedList.cpp LinkedList.h LoopDetect.cpp LoopDetect.h)