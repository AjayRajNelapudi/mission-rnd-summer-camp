#include <stdio.h>
#include "LinkedList.h"
#include "LoopDetect.h"

int main() {
    int elements[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int size = sizeof(elements) / sizeof(elements[0]);
    Node *listWithLoop = constructLinkedListWithLoop(elements, size, 6);

    Node *loopBeginNode = getLoopBeginNode(listWithLoop);
    if (loopBeginNode == NULL) {
        printf("List does not have a loop\n");
    } else {
        printf("Loop begins at %d\n", loopBeginNode->data);
    }

    return 0;
}