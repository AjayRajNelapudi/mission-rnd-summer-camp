//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#include "linkedlist.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

LLNode *makeLLNode(char *word) {
    LLNode *newNode = (LLNode *)malloc(sizeof(LLNode));
    strcpy(newNode->word, word);
    newNode->next = NULL;

    return newNode;
}

LLNode *append(LLNode *head, char *word) {
    LLNode *newNode = makeLLNode(word);

    if (head == NULL) {
        return newNode;
    }

    LLNode *currentNode = head;
    while (currentNode->next != NULL) {
        currentNode = currentNode->next;
    }
    currentNode->next = newNode;

    return head;
}

void printList(LLNode *head) {
    while (head != NULL) {
        printf("%s\t", head->word);
        head = head->next;
    }
    printf("\n");
}

void fprintList(LLNode *head, FILE *file) {
    while (head != NULL) {
        fprintf(file, "%s\n", head->word);
        head = head->next;
    }
    fprintf(file, "\n\n\n");
}

bool areAnagrams(char* str1, char* str2) {
    int count1[26] = { 0 };
    int count2[26] = { 0 };
    int i;

    for (i = 0; str1[i] && str2[i]; i++) {
        count1[str1[i]]++;
        count2[str2[i]]++;
    }


    if (str1[i] || str2[i])
        return false;

    for (i = 0; i < 26; i++)
        if (count1[i] != count2[i])
            return false;

    return true;
}

void searchLinkedListForAnagrams(LLNode *bigWords, char *input) {
    while (bigWords != NULL) {
        if (areAnagrams(input, bigWords->word)) {
            printf("%s\n", bigWords->word);
        }
        bigWords = bigWords->next;
    }
}