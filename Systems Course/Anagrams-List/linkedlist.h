//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#ifndef ANAGRAMS_LIST_LINKEDLIST_H
#define ANAGRAMS_LIST_LINKEDLIST_H

#include <stdio.h>

typedef struct LinkedListNode {
    char word[100];
    struct LinkedListNode *next;
} LLNode;

LLNode *append(LLNode *head, char *word);
void printList(LLNode *head);
void fprintList(LLNode *head, FILE *file);
void searchLinkedListForAnagrams(LLNode *bigWords, char *input);

#endif //ANAGRAMS_LIST_LINKEDLIST_H
