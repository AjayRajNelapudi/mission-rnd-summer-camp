//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#ifndef ANAGRAMS_LIST_BINARYSEARCHTREE_H
#define ANAGRAMS_LIST_BINARYSEARCHTREE_H
#include "linkedlist.h"

typedef struct BinarySearchTreeNode {
    unsigned long key;
    LLNode *anagrams;
    struct BinarySearchTreeNode *left, *right;
} BSTNode;

BSTNode *insertIntoBST(BSTNode *root, unsigned long long key, char *word);
BSTNode *searchBST(BSTNode *root, unsigned long long key);
void printToFile(BSTNode *root, FILE *file);

#endif //ANAGRAMS_LIST_BINARYSEARCHTREE_H
