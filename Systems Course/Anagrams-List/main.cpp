#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "linkedlist.h"
#include "binarysearchtree.h"

unsigned long long generateKey(char *word) {
    int length = strlen(word);

    char *newWord = (char *)malloc(sizeof(char) * length);
    strcpy(newWord, word);

    int *letters = (int *)calloc(26, sizeof(int));
    for (int i = 0; i < length; i++) {
        if (newWord[i] >= 'A' && newWord[i] <= 'Z') {
            newWord[i] += 'a' - 'A';
        }

        int index = newWord[i] - 97;
        letters[index] += 1;
    }

    int base = 5;
    unsigned long long key = 0;
    for (int i = 0; i < 26; i++) {
        key += pow(base, i + 1) * letters[i];
    }
    free(letters);

    return key;
}

BSTNode *preprocess(FILE *file) {
    BSTNode *root = NULL;

    char *word = (char *)malloc(sizeof(char) * 50);
    while (fscanf(file, "%s", word) != EOF) {
        unsigned long long key = generateKey(word);
        root = insertIntoBST(root, key, word);
    }
    free(word);

    return root;
}

int main() {
    char filename[] = "/Users/ajayraj/Documents/MRND-Summer/Anagrams-List/words.txt";
    FILE *file = fopen(filename, "r");
    printf("Pre-processing in progress...\n");
    BSTNode *root = preprocess(file);
    printf("Pre-processing complete.\n");
    fclose(file);

    file = fopen("processed.txt", "w");
    printToFile(root, file);
    fclose(file);

    LLNode *bigWords = NULL;
    char *input = (char *)malloc(sizeof(char) * 50);
    while (1) {
        printf("> ");
        scanf("%s", input);

        if (strcmp(".exit", input) == 0) {
            break;
        }

        unsigned long long key = generateKey(input);
        if (key == 0) {
            bigWords = append(bigWords, input);
            continue;
        }

        BSTNode *keyNode = searchBST(root, key);
        if (keyNode == NULL) {
            searchLinkedListForAnagrams(bigWords, input);
        } else {
            printList(keyNode->anagrams);
            searchLinkedListForAnagrams(bigWords, input);
        }
    }

    free(input);
    return 0;
}