//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#include "linkedlist.h"
#include "binarysearchtree.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

BSTNode *makeBSTNode(unsigned long long key) {
    BSTNode *newNode = (BSTNode *)malloc(sizeof(BSTNode));
    newNode->key = key;
    newNode->anagrams = NULL;
    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}

BSTNode *insertIntoBST(BSTNode *root, unsigned long long key, char *word) {
    if (root == NULL) {
        BSTNode *newNode = makeBSTNode(key);
        newNode->anagrams = append(newNode->anagrams, word);
        return newNode;
    }

    if (key < root->key) {
        root->left = insertIntoBST(root->left, key, word);
    } else if (key > root->key) {
        root->right = insertIntoBST(root->right, key, word);
    } else {
        root->anagrams = append(root->anagrams, word);
    }

    return root;
}

BSTNode *searchBST(BSTNode *root, unsigned long long key) {
    if (root == NULL) {
        return NULL;
    }

    if (key == root->key) {
        return root;
    }

    if (key < root->key) {
        return searchBST(root->left, key);
    }

    return searchBST(root->right, key);
}

void printToFile(BSTNode *root, FILE *file) {
    if (root == NULL) {
        return;
    }

    printToFile(root->left, file);
    fprintf(file, "KEY: %lld\n", root->key);
    fprintList(root->anagrams, file);
    printToFile(root->right, file);
}

