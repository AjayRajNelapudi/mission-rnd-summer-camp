#include <stdio.h>

long arraySum(int *arr, int n) {
    return n <= 0 ? 0 : arr[n - 1] + arraySum(arr, n - 1);
}

int main() {
    int arr[] = {1, 2, 3, 4, 5};
    long sum = arraySum(arr, 5);
    printf("Sum: %ld", sum);

    return 0;
}