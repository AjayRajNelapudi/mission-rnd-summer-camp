#include <stdio.h>
const int limit = 1000000007;

int pow(int a, int k) {
    if (k == 0) {
        return 1;
    }

    int value = (pow(a, k/2) * pow(a, k/2)) % limit;
    if (k & 1) {
        value *= a % limit;
    }

    return value;
}

int main() {
    int res = pow(10, 2);
    printf("%d ", res);

    return 0;
}