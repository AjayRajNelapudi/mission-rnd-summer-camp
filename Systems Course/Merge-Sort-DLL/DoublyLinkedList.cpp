//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#include <stdio.h>
#include <stdlib.h>
#include "DoublyLinkedList.h"

Node *makeNode(int data) {
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->data = data;
    newNode->prev = NULL;
    newNode->next = NULL;

    return newNode;
}

Node *append(Node *head, int data) {
    Node *newNode = makeNode(data);

    if (head == NULL) {
        return newNode;
    }

    Node *currentNode = head;

    while (currentNode->next != NULL) {
        currentNode = currentNode->next;
    }

    currentNode->next = newNode;
    newNode->prev = currentNode;

    return head;
}

Node *constructDoublyLinkedListFromArray(int *arr, int n) {
    if (arr == NULL) {
        return NULL;
    }

    Node *DoublyLinkedList = makeNode(arr[0]);
    Node *currentNode = DoublyLinkedList;
    for (int i = 1; i < n; i++) {
        currentNode->next = makeNode(arr[i]);
        currentNode->next->prev = currentNode;
        currentNode = currentNode->next;
    }

    return DoublyLinkedList;
}

void freeDoubyLinkedList(Node *head) {
    if (head == NULL) {
        return;
    }

    while (head != NULL) {
        Node *temp = head;
        head = head->next;
        free(temp);
    }
}

int listLen(Node *head) {
    int len = 0;

    Node *currentNode = head;
    while (currentNode != NULL) {
        len++;
        currentNode = currentNode->next;
    }

    return len;
}

void printList(Node *head) {
    if (head == NULL) {
        return;
    }

    Node *currentNode = head;
    while (currentNode != NULL) {
        printf("%d ", currentNode->data);
        currentNode = currentNode->next;
    }
    printf("\n");
}

Node *getMiddleNodeOfDoublyLinkedList(Node *head) {
    if (head == NULL) {
        return NULL;
    }

    int len = 0;
    Node *currentNode = head;
    while (currentNode->next != NULL) {
        len++;
        currentNode = currentNode->next;
    }

    for (int i = 0; i < (len + 1) / 2; i++) {
        currentNode = currentNode->prev;
    }

    return currentNode;
}