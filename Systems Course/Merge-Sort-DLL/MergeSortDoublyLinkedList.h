//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#ifndef MERGE_SORT_DLL_MERGESORTDOUBLYLINKEDLIST_H
#define MERGE_SORT_DLL_MERGESORTDOUBLYLINKEDLIST_H

#include "DoublyLinkedList.h"

void mergeSort(Node **head);

#endif //MERGE_SORT_DLL_MERGESORTDOUBLYLINKEDLIST_H
