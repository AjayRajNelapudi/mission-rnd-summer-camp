#include <stdio.h>
#include "DoublyLinkedList.h"
#include "MergeSortDoublyLinkedList.h"

int main() {
    int elements[] = {9, 2, 3, 8, 1, 6, 7, 5};
    int size = sizeof(elements) / sizeof(elements[0]);
    Node *list = constructDoublyLinkedListFromArray(elements, size);
    printList(list);

    mergeSort(&list);
    printList(list);

    return 0;
}