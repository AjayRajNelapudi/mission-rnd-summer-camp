//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#ifndef MERGE_SORT_DLL_DOUBLYLINKEDLIST_H
#define MERGE_SORT_DLL_DOUBLYLINKEDLIST_H

typedef struct DoublyLinkedList {
    int data;
    struct DoublyLinkedList *prev;
    struct DoublyLinkedList *next;
} Node;

Node *makeNode(int data);
Node *append(Node *head, int data);
Node *constructDoublyLinkedListFromArray(int *arr, int n);
void freeDoubyLinkedList(Node *head);
int listLen(Node *head);
void printList(Node *head);
Node *getMiddleNodeOfDoublyLinkedList(Node *head);

#endif //MERGE_SORT_DLL_DOUBLYLINKEDLIST_H
