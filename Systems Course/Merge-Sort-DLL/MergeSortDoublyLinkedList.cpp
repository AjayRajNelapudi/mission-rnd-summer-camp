//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#include <stddef.h>
#include "MergeSortDoublyLinkedList.h"
#include "DoublyLinkedList.h"
#include <stdio.h>

Node* mergeTwoSortedLists(Node *firstPart, Node *secondPart) {
    if (firstPart == NULL) {
        return secondPart;
    }

    if (secondPart == NULL) {
        return firstPart;
    }

    Node *sortedList = NULL;
    Node *sortedListHead = NULL;
    if (firstPart->data < secondPart->data) {
        sortedList = firstPart;
        firstPart = firstPart->next;
    } else {
        sortedList = secondPart;
        secondPart = secondPart->next;
    }
    sortedListHead = sortedList;

    while (firstPart != NULL && secondPart != NULL) {
        if (firstPart->data < secondPart->data) {
            sortedList->next = firstPart;
            firstPart->prev = sortedList;

            firstPart = firstPart->next;
        } else {
            sortedList->next = secondPart;
            secondPart->prev = sortedList;

            secondPart = secondPart->next;
        }
        sortedList = sortedList->next;
    }

    while (firstPart != NULL) {
        sortedList->next = firstPart;

        firstPart = firstPart->next;
        sortedList = sortedList->next;
    }

    while (secondPart != NULL) {
        sortedList->next = secondPart;

        secondPart = secondPart->next;
        sortedList = sortedList->next;
    }

    return sortedListHead;
}

Node *getSecondPart(Node *head) {
    if (head == NULL) {
        return NULL;
    }

    Node *middleNode = getMiddleNodeOfDoublyLinkedList(head);
    Node *secondPart = middleNode->next;

    if (secondPart != NULL) {
        secondPart->prev = NULL;
        middleNode->next = NULL;
    }

    return secondPart;
}

void mergeSort(Node **head) {
    if (*head == NULL || head[0]->next == NULL) {
        return;
    }

    Node *firstPart = *head;
    Node *secondPart = getSecondPart(*head);

    mergeSort(&firstPart);
    mergeSort(&secondPart);
    *head = mergeTwoSortedLists(firstPart, secondPart);
}