//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#ifndef FILE_SYSTEM_BLOCKS_DISKUTILITIES_H
#define FILE_SYSTEM_BLOCKS_DISKUTILITIES_H

#define BLOCK_SIZE 16 * 1024
#define HDD_SIZE 100 * 1024 * 1024
#define METADATA_BLOCKS 400
#define RECORDS 16 * 6400
#define BLOCKS 6000
#define MAGIC_NUMBER 0x444E524D
#define DISK "/Users/ajayraj/Documents/MRND-Summer/File-System-Blocks/HardDisk.hdd"

typedef struct MetadataRecord {
    char filename[20];
    unsigned int startBlock;
    unsigned int noOfBlocks;
    unsigned int fileLength;
} Record;

typedef struct MetadataTable {
    Record records[RECORDS];
    unsigned int files;
    bool freeBlocks[BLOCKS];
} Metadata;

#endif //FILE_SYSTEM_BLOCKS_DISKUTILITIES_H
