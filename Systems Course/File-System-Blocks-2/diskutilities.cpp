//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

//0x444E24D

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "diskutilities.h"

void addToMetadata(Metadata *metadata, char *filename, unsigned int startBlock, unsigned int noOfBlocks, unsigned int fileLength) {
    int index = metadata->files;
    strcpy(metadata->records[index].filename, filename);
    metadata->records[index].startBlock = startBlock;
    metadata->records[index].noOfBlocks = noOfBlocks;
    metadata->records[index].fileLength = fileLength;
    metadata->files++;
}

void writeToBlock(unsigned int blockIndex, char *buffer, FILE *destination, unsigned int size) {
    if (buffer == NULL || destination == NULL) {
        return;
    }

    unsigned int offset = BLOCK_SIZE * blockIndex;
    fseek(destination, offset, SEEK_SET);
    fwrite(buffer, sizeof(char), size, destination);
    free(buffer);
    buffer = NULL;
}

long int getFileSize(char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        return -1;
    }

    fseek(file, 0, SEEK_END);
    long int size = ftell(file);
    fclose(file);

    return size;
}

unsigned int getFreeIndexForBlocks(Metadata *metadata, unsigned int noOfBlocks) {
    if (noOfBlocks > BLOCKS) {
        return -1;
    }

    int freeIndices = 0;
    unsigned int i = 0, j = 0;
    while (i < BLOCKS && j < BLOCKS) {
        if (metadata->freeBlocks[i] == false) {
            i++;
            if (abs(i - j) == noOfBlocks) {
                return j;
            }
        } else {
            j = i;
        }
    }

    return -1;
}

void importFile(Metadata *metadata, char *source, char *destination) {
    if (metadata == NULL || source == NULL || destination == NULL) {
        return;
    }

    FILE *sourceFile = fopen(source, "rb");
    long int sourceFileSize = getFileSize(source);
    FILE *destinationFile = fopen(DISK, "rb+");

    if (sourceFile == NULL || destinationFile == NULL) {
        return;
    }

    unsigned int index = getFreeIndexForBlocks(metadata, ceil(sourceFileSize / BLOCK_SIZE));
    if (index == -1) {
        return;
    }

    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    while (fread(buffer, sizeof(char), BLOCK_SIZE, sourceFile) != 0) {
        writeToBlock(index, buffer, destinationFile, BLOCK_SIZE);
        metadata->freeBlocks[index] = true;
        index++;
    }

    if (fread(buffer, sizeof(char), sourceFileSize % BLOCK_SIZE, sourceFile) != 0) {
        writeToBlock(index, buffer, destinationFile, sourceFileSize % BLOCK_SIZE);
        metadata->freeBlocks[index] = true;
    }

    addToMetadata(metadata, destination, index, ceil(sourceFileSize / BLOCK_SIZE), sourceFileSize);

    free(buffer);
    fclose(destinationFile);
    fclose(sourceFile);
}

unsigned int getFileStartBlock(Metadata *metadata, char *filename) {
    if (filename == NULL) {
        return -1;
    }

    for (int i = 0; i < metadata->files; i++) {
        if (strcmp(metadata->records[i].filename, filename) == 0) {
            return metadata->records[i].startBlock;
        }
    }

    return -1;
}

unsigned int getFileLength(Metadata *metadata, char *filename) {
    if (filename == NULL) {
        return -1;
    }

    for (int i = 0; i < metadata->files; i++) {
        if (strcmp(metadata->records[i].filename, filename) == 0) {
            return metadata->records[i].startBlock;
        }
    }

    return -1;
}

void exportFile(Metadata *metadata, char *source, char *destination) {
    if (metadata == NULL || source == NULL || destination == NULL) {
        return;
    }

    FILE *sourceFile = fopen(DISK, "rb+");
    FILE *destinationFile = fopen(destination, "wb");
    if (sourceFile == NULL || destinationFile == NULL) {
        return;
    }

    unsigned int startBlock = getFileStartBlock(metadata, source);
    unsigned int fileLength = getFileLength(metadata, source);

    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    for (int i = 0; i < fileLength / BLOCK_SIZE; i++) {
        readFromBlock(startBlock, buffer, sourceFile, BLOCK_SIZE);
    }

    fclose(sourceFile);
    fclose(destinationFile);
}