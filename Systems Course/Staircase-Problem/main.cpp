#include <stdio.h>

long countWays(long steps, long k) {
    if (steps == 0 || steps == 1) {
        return steps;
    }

    long possibleWays = 0;
    for (long i = 1; i <= steps && i <= k; i++) {
        possibleWays += countWays(steps - i, k);
    }

    return possibleWays;
}

int main() {
    long steps = 4, maxStepLength = 2;
    long result = countWays(steps + 1, maxStepLength);
    printf("%ld\n", result);

    return 0;
}