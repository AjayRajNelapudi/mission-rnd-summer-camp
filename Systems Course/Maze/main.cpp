#include <stdio.h>
#include <stdlib.h>
#include "mazeutililities.h"

bool nextMove(int **maze, int rows, int columns,
              int nextI, int nextJ,
              int startI, int startJ, int endI,
              int endJ, int **path,
              int prevI, int prevJ);
bool solveMaze(int **maze, int rows, int columns,
           int startI, int startJ, int endI,
           int endJ, int **path,
           int prevI, int prevJ);

bool nextMove(int **maze, int rows, int columns,
              int nextI, int nextJ,
              int startI, int startJ, int endI,
              int endJ, int **path,
              int prevI, int prevJ) {

    return !(nextI == prevI && nextJ == prevJ) &&
           solveMaze(maze, rows, columns, nextI, nextJ, endI, endJ, path, startI, startJ);
}

bool solveMaze(int **maze, int rows, int columns,
            int startI, int startJ, int endI,
            int endJ, int **path,
            int prevI, int prevJ) {

    if (maze == NULL || path == NULL) {
        return false;
    }

    if (startI < 0 || startI >= rows || startJ < 0 || startJ >= columns) {
        return false;
    }

    if (maze[startI][startJ] == 0) {
        return false;
    }

    if (path[startI][startJ] == 1) {
        return false;
    }

    if (startI == endI && startJ == endJ) {
        path[startI][startJ] = 1;
        return true;
    }

    if (nextMove(maze, rows, columns, startI + 1, startJ, startI, startJ, endI, endJ, path, prevI, prevJ)) {
        path[startI][startJ] = 1;
        return true;
    }

    if (nextMove(maze, rows, columns, startI, startJ + 1, startI, startJ, endI, endJ, path, prevI, prevJ)) {
        path[startI][startJ] = 1;
        return true;
    }

    if (nextMove(maze, rows, columns, startI - 1, startJ, startI, startJ, endI, endJ, path, prevI, prevJ)) {
        path[startI][startJ] = 1;
        return true;
    }

    if (nextMove(maze, rows, columns, startI, startJ - 1, startI, startJ, endI, endJ, path, prevI, prevJ)) {
        path[startI][startJ] = 1;
        return true;
    }

    return false;
}

int main() {
    int rows = 5;
    int columns = 6;

    int blocked[][2] = {{0, 1}, {2, 0}, {1, 4}, {3, 4}, {3, 0}, {0, 5}, {4, 3}};
    int **maze = constructMaze(rows, columns, blocked, sizeof(blocked) / sizeof(blocked[0]));
    int **path = constructMaze(rows, columns, blocked, 0);

    printf("Maze:\n");
    printMaze(maze, rows, columns); printf("\n");

    solveMaze(maze, rows, columns, 0, 0, 4, 5, path, -1, -1);

    printf("Path:\n");
    printMaze(path, rows, columns);

    return 0;
}