//
// Created by Ajay Raj Nelapudi on 2019-05-28.
//

#ifndef MAZE_MAZEUTILILITIES_H
#define MAZE_MAZEUTILILITIES_H

int** constructMaze(int rows, int columns, int blocked[][2], int length);
void printMaze(int **maze, int rows, int columns);

#endif //MAZE_MAZEUTILILITIES_H
