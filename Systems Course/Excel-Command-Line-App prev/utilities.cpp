//
// Created by Ajay Raj Nelapudi on 2019-05-27.
//

#include "utilities.h"
#include <stddef.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

bool isValidString(char *string) {
    return (string != NULL && *string != '\0');
}

void readCellAndValue(char *line, int lineLength, char *cell, char *value) {
    if (!isValidString(line)) {
        return;
    }

    if (cell == NULL || value == NULL) {
        return;
    }

    int i = 0;
    while (line[i] != '\0' && line[i] == ' ') {
        i++;
    }

    int cellIndex = 0;
    while (line[i] != '\0' && line[i] != ' ' && line[i] != '=') {
        cell[cellIndex++] = line[i];
        i++;
    }
    cell[cellIndex] = '\0';

    while (line[i] != '\0' && line[i] == ' ') {
        i++;
    }

    while (line[i] != '\0' && line[i] == '=') {
        i++;
    }

    while (line[i] != '\0' && line[i] == ' ') {
        i++;
    }

    int valueIndex = 0;
    while (line[i] != '\0' && line[i] != ' ') {
        value[valueIndex++] = line[i];
        i++;
    }
    value[valueIndex] = '\0';
}

void separateRowAndColumn(char *cell, char *row, char *column) {
    if (!isValidString(cell)) {
        return;
    }

    if (row == NULL) {
        return;
    }

    if (column == NULL) {
        return;
    }

    int i = 0;
    while (cell[i] != '\0' && isspace(cell[i])) {
        i++;
    }

    int columnIndex = 0;
    while (cell[i] != '\0' && isalpha(cell[i])) {
        column[columnIndex++] = cell[i];
        i++;
    }
    column[columnIndex] = '\0';

    int rowIndex = 0;
    while (cell[i] != '\0' && isdigit(cell[i])) {
        row[rowIndex++] = cell[i];
        i++;
    }
    row[rowIndex] = '\0';
}

int getIndex(char *rowString) {
    if (rowString == NULL || *rowString == '\0') {
        return -1;
    }

    int index = rowString[0];
    if (index >= 65 && index <= 65 + 26) {
        return index - 65;
    } else if (index >= 97 && index <= 97 + 26) {
        return index - 97;
    }

    return -1;
}

int* getRowAndColumn(char *cell) {
    if (!isValidString(cell)) {
        return NULL;
    }

    int *indices = (int *)malloc(sizeof(int) * 3);
    char *rowString = (char *)malloc(5 * sizeof(char));
    char *columnString = (char *)malloc(5 * sizeof(char));
    separateRowAndColumn(cell, rowString, columnString);

    indices[0] = atoi(rowString) - 1;
    indices[1] = getIndex(columnString);
    indices[2] = '\0';

    return indices;
}

void getFilename(char *line, char *filename) {
    if (line == NULL || *line == '\0') {
        return;
    }

    int i = 0;
    int filenameIndex = 0;
    while (line[i] != '\0' && line[i] == ' ') {
        i++;
    }

    while (line[i] != '\0') {
        filename[filenameIndex++] = line[i];
        i++;
    }
    filename[filenameIndex] = '\0';
}

int nextChar(char *str) {
    if (!isValidString(str)) {
        return NULL;
    }

    int i = 0;
    for(i = 0; str[i] != '\0' && !isspace(str[i]); i++);

    return i;
}

int fetchToken(char *str, char *word) {
    if (!isValidString(str)) {
        return -1;
    }

    if (word == NULL) {
        return -1;
    }

    int i = nextChar(str);
    int index = 0;
    while (str[i] != '\0' && str[i] != ' ') {
        word[index++] = str[i];
        i++;
    }
    word[index] = '\0';

    return index;
}

int countOperators(char *expression) {
    if (!isValidString(expression)) {
        return -1;
    }

    int operators = 0;
    for (int i = 0; expression[i] != '\0'; i++) {
        if (expression[i] == '+' || expression[i] == '-' || expression[i] == '*' || expression[i] == '/') {
            operators++;
        }
    }

    return operators;
}

int compute(int valueA, int valueB, char operation) {
    switch(operation) {
        case '+':
            return valueA + valueB;

        case '-':
            return valueA - valueB;

        case '*':
            return valueA * valueB;

        case '+/':
            return valueA / valueB;

        default:
            return -1;
    }
}

void printLine(int length) {
    for (int i = 0; i < length; i++) {
        printf("_");
    }
    printf("\n");
}

char* reverse(char *string) {
    if (!isValidString(string)) {
        return NULL;
    }

    int stringLength = strlen(string);
    char *reverseString = (char *)malloc(sizeof(char) * stringLength);

    int begin = 0;
    int end = stringLength - 1;
    while (end >= 0) {
        reverseString[begin++] = string[end--];
    }

    return reverseString;
}

void addExtension(char *filename) {
    if (!isValidString(filename)) {
        return;
    }

    char extension[] = ".csv";
    char *filenameReverse = reverse(filename);
    char *extensionReverse = reverse(extension);
    strlower(filenameReverse);

    if (stringMatchN(filenameReverse, extensionReverse, 4)) {
        return;
    }

    int filenameLength = strlen(filename);
    for (int i = 0; i < strlen(extension); i++) {
        filename[filenameLength + i] = extension[i];
    }
}

void strlower(char *s) {
    int c = 0;

    while (s[c] != '\0') {
        if (s[c] >= 'A' && s[c] <= 'Z') {
            s[c] = s[c] + 32;
        }
        c++;
    }
}

bool stringMatchN(char *line, char *key, int len) {
    if (strlen(line) < len) {
        return false;
    }

    if (strlen(key) < len) {
        return false;
    }

    char *commandCopy = (char *)calloc(len + 1, sizeof(char));
    strcpy(commandCopy, line);
    commandCopy[len] = '\0';

    char *keyCopy = (char *)calloc(len + 1, sizeof(char));
    strcpy(keyCopy, key);
    keyCopy[len] = '\0';

    strlower(commandCopy);
    strlower(keyCopy);

    if (strcmp(commandCopy, keyCopy) == 0) {
        return true;
    }

    return false;
}

void popFront(char *command, int offset) {
    if (command == NULL) {
        return;
    }

    if (strlen(command) <= offset) {
        return;
    }

    int end = 0;
    int len = strlen(command);
    for (int i = offset; i < len; i++) {
        command[end++] = command[i];
    }
    command[end] = '\0';
}

char *readCommand() {
    char *command = (char *)malloc(20 * sizeof(char));
    scanf("%[^\n]s", command);
    char extra;
    scanf("%c", &extra);

    return command;
}

bool isNumber(char *string) {
    if (!isValidString(string)) {
        return false;
    }

    int len = strlen(string);
    for (int i = 0; i < len; i++) {
        if (!isdigit(string[i])) {
            return false;
        }
    }

    return true;
}