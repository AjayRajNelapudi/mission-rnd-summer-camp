#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Data {
    char name[8];
    int age;
    float height;
};

void usefwriteAndfread() {
    int number1 = 0x31323334;
    FILE *file = fopen("sample.bin", "wb");
    fwrite(&number1, sizeof(int), 1, file);
    fclose(file);

    int number2;
    file = fopen("sample.bin", "rb");
    fread(&number2, sizeof(int), 1, file);
    fclose(file);

    printf("number1: %d\tnumber2: %d", number1, number2);
}

void usefwriteAndfprintf() {
    int n = 0x31323334;
    int m = 4321;

    FILE *file = fopen("fwrite.bin", "wb");
    fwrite(&m, sizeof(int), 1, file);
    fclose(file);

    file = fopen("fprintf.txt", "w");
    fprintf(file, "%d\n", n);
    fclose(file);
}

void fwriteArray() {
    int name[] = { 1920409673, 543519849, 543449442, 1701080931, 1767990304, 3045740};
    FILE *file = fopen("fwriteArray.bin", "wb");
    fwrite(name, sizeof(int), sizeof(name) / sizeof(name[0]), file);
    fclose(file);
}

void fwriteStructure() {
    struct Data *data = (struct Data *)malloc(sizeof(struct Data));
    strcpy(data->name, "Ajay");
    data->age = 20;
    data->height = 178.3;

    FILE *file = fopen("fwriteStructure.bin", "wb");
    fwrite(data, sizeof(struct Data), 1, file);
    fclose(file);

    file = fopen("fwriteStructure.bin", "rb");
    struct Data *readData = (struct Data *)malloc(sizeof(struct Data));
    printf("Size of structure: %d\n", fread(readData, sizeof(struct Data), 1, file));
    printf("%s %d %d", readData->name, readData->age, readData->height);
    fclose(file);
}

int main() {
    //usefwriteAndfread();
    //usefwriteAndfprintf();
    fwriteArray();
    //fwriteStructure();
    return 0;
}