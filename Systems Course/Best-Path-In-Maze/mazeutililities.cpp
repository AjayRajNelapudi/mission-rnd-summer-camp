//
// Created by Ajay Raj Nelapudi on 2019-05-28.
//

#include "mazeutililities.h"
#include <stdlib.h>
#include <stdio.h>

int** constructMaze(int rows, int columns, int blocked[][2], int length) {
    int **maze = (int **)calloc(rows, sizeof(int *));
    for (int i = 0; i < rows; i++) {
        maze[i] = (int *)calloc(columns, sizeof(int));
    }

    if (length > 0) {
        for (int i = 0; i < length; i++) {
            maze[blocked[i][0]][blocked[i][1]] = 1;
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                maze[i][j] = !maze[i][j];
            }
        }
    }

    return maze;
}

void printMaze(int **maze, int rows, int columns) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            printf("%d ", maze[i][j]);
        }
        printf("\n");
    }
}

Path *makeNode(int x, int y) {
    Path *newNode = (Path *)malloc(sizeof(Path));
    newNode->x = x;
    newNode->y = y;
}

void addToPath(PathData *pathdata, int x, int y) {

}