#include <stdio.h>
#include <stdlib.h>
#include "mazeutililities.h"

int nextMove(int **maze, int rows, int columns,
              int nextI, int nextJ,
              int startI, int startJ, int endI,
              int endJ, int **path,
              int prevI, int prevJ);
int solveMaze(int **maze, int rows, int columns,
           int startI, int startJ, int endI,
           int endJ, int **path,
           int prevI, int prevJ);

int min(int a, int b);
int minOf(int a, int b, int c, int d);

int min(int a, int b) {
    return a > b ? a : b;
}

int minOf(int a, int b, int c, int d) {
    return min(a, min(b, min(c, d)));
}

int nextMove(int **maze, int rows, int columns,
              int nextI, int nextJ,
              int startI, int startJ, int endI,
              int endJ, int **path,
              int prevI, int prevJ) {

    if (!(nextI == prevI && nextJ == prevJ)) {
        return solveMaze(maze, rows, columns, nextI, nextJ, endI, endJ, path, startI, startJ);
    }
    return 0;
}

int solveMaze(int **maze, int rows, int columns,
            int startI, int startJ, int endI,
            int endJ, int **path,
            int prevI, int prevJ) {

    if (maze == NULL || path == NULL) {
        return 0;
    }

    if (startI < 0 || startI >= rows || startJ < 0 || startJ >= columns) {
        return 0;
    }

    if (maze[startI][startJ] == 0) {
        return 0;
    }

    if (path[startI][startJ] == 1) {
        return 0;
    }

    if (startI == endI && startJ == endJ) {
        path[startI][startJ] = 1;
        return 1;
    }

    int south = nextMove(maze, rows, columns, startI + 1, startJ, startI, startJ, endI, endJ, path, prevI, prevJ);
    int east = nextMove(maze, rows, columns, startI, startJ + 1, startI, startJ, endI, endJ, path, prevI, prevJ);
    int north = nextMove(maze, rows, columns, startI - 1, startJ, startI, startJ, endI, endJ, path, prevI, prevJ);
    int west = nextMove(maze, rows, columns, startI, startJ - 1, startI, startJ, endI, endJ, path, prevI, prevJ);

    int miniumDistance = minOf(north, east, west, south);

}

int main() {
    int rows = 5;
    int columns = 6;

    int blocked[][2] = {{0, 1}, {2, 0}, {1, 4}, {3, 4}, {3, 0}, {0, 5}, {4, 3}};
    int **maze = constructMaze(rows, columns, blocked, sizeof(blocked) / sizeof(blocked[0]));
    int **path = constructMaze(rows, columns, blocked, 0);

    printf("Maze:\n");
    printMaze(maze, rows, columns); printf("\n");

    solveMaze(maze, rows, columns, 0, 0, 4, 5, path, -1, -1);

    printf("Path:\n");
    printMaze(path, rows, columns);

    return 0;
}