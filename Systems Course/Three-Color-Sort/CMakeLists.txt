cmake_minimum_required(VERSION 3.14)
project(Three_Color_Sort)

set(CMAKE_CXX_STANDARD 14)

add_executable(Three_Color_Sort main.cpp ThreeColorSort.cpp ThreeColorSort.h)