//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#ifndef THREE_COLOR_SORT_THREECOLORSORT_H
#define THREE_COLOR_SORT_THREECOLORSORT_H

void threeColorSort(int *arr, int len);

#endif //THREE_COLOR_SORT_THREECOLORSORT_H
