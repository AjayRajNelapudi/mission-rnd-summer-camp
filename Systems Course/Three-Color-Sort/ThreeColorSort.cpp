//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#include "ThreeColorSort.h"
#include <stddef.h>

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void threeColorSort(int *arr, int len) {
    if (arr == NULL) {
        return;
    }

    int zeroPtr = 0;
    int onePtr = 0;
    int twoPtr = len - 1;

    while (onePtr <= twoPtr) {
        if (arr[onePtr] == 0) {
            swap(&arr[zeroPtr], &arr[onePtr]);
            zeroPtr++; onePtr++;
        } else if (arr[onePtr] == 1) {
            onePtr++;
        } else if(arr[onePtr] == 2) {
            swap(&arr[onePtr], &arr[twoPtr]);
            twoPtr--;
        }
    }
}