#include <stdio.h>
#include <stdlib.h>
#include "ThreeColorSort.h"

int main() {
    int arr[] = {2, 1, 2, 0, 0, 1, 2, 1};
    int len = sizeof(arr) / sizeof(arr[0]);
    threeColorSort(arr, len);

    printf("The sorted array is: ");
    for(int i = 0; i < len; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return 0;
}
