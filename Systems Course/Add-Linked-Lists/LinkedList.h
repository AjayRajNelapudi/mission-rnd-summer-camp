//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#ifndef ADD_LINKED_LISTS_LINKEDLIST_H
#define ADD_LINKED_LISTS_LINKEDLIST_H

typedef struct LinkedList {
    int data;
    struct LinkedList *next;
} Node;

Node *makeNode(int data);
Node *append(Node *head, int data);
void freeLinkedList(Node *head);
Node *constructLinkedListFromArray(int *elements, int n);
Node *constructLinkedListWithLoop(int *elements, int n, int loopBeginPosition);
Node *reverseLinkedList(Node *head);
int listLen(Node *head);
void printList(Node *head);

#endif //ADD_LINKED_LISTS_LINKEDLIST_H
