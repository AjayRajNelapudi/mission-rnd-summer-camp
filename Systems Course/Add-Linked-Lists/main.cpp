#include <stdio.h>
#include <stdlib.h>
#include "LinkedList.h"

int addTwoNodes(Node *num1, int len1, Node *num2, int len2, Node *result) {
    if (num1 == NULL && num2 == NULL) {
        return 0;
    }

    int carry = 0;
    int num1Digit = 0, num2Digit = 0;

    if (!(len1 == 1 && len2 == 1)) {
        result->next = makeNode(0);
    }

    if (len1 > len2) {
        carry = addTwoNodes(num1->next, len1 - 1, num2, len2, result->next);
        num1Digit = num1->data;
        num2Digit = 0;
    } else if (len1 < len2) {
        carry = addTwoNodes(num1, len1, num2->next, len2 - 1, result->next);
        num1Digit = 0;
        num2Digit = num2->data;
    } else {
        carry = addTwoNodes(num1->next, len1 - 1, num2->next, len2 - 1, result->next);
        num1Digit = num1->data;
        num2Digit = num2->data;
    }

    int sum = num1Digit + num2Digit + carry;
    result->data = sum % 10;
    return sum / 10;
}

Node *sumOfNumbers(Node *num1, Node *num2) {
    int len1 = listLen(num1);
    int len2 = listLen(num2);

    Node *result = makeNode(0);
    result->next = makeNode(0);
    int carry = addTwoNodes(num1, len1, num2, len2, result->next);
    result->data = carry;
    if (carry == 0) {
        result = result->next;
    }

    return result;
}

int main() {
    int num1Array[] = {7, 2, 5};
    Node *num1 = constructLinkedListFromArray(num1Array, 3);

    int num2Array[] = {9, 0, 5, 2, 6};
    Node *num2 = constructLinkedListFromArray(num2Array, 5);

    Node *result = sumOfNumbers(num1, num2);
    printList(result);

    return 0;
}