#include <stdio.h>
#include <stdlib.h>

void printInverseArray(int *arr, int n) {
    char inverseMap[] = "012Eh59L86";
    bool firstNonZeroOccured = false;

    for (int i = 0; i < n; i++) {
        if (arr[i] != 0) {
            firstNonZeroOccured = true;
        }

        if (firstNonZeroOccured) {
            printf("%c", inverseMap[arr[i]]);
        }
    }

    if (!firstNonZeroOccured) {
        printf("0");
    }
    printf("\n");
}

void printArray(int *arr, int n) {
    bool firstNonZeroOccured = false;

    for (int i = 0; i < n; i++) {
        if (arr[i] != 0) {
            firstNonZeroOccured = true;
        }

        if (firstNonZeroOccured) {
            printf("%d", arr[i]);
        }
    }

    if (!firstNonZeroOccured) {
        printf("0");
    }
    printf("\n");
}

void printToN(int *arr, int n, int index) {
    if (index + 1 >= n) {
        printInverseArray(arr, n - 1);
        return;
    }

    for (int i = 0; i <= 9; i++) {
        arr[index] = i;
        printToN(arr, n, index + 1);
    }
}

int main() {
    int n = 3;
    int *arr = (int *)malloc(sizeof(int) * (n - 1));
    printToN(arr, n, 0);

    return 0;
}