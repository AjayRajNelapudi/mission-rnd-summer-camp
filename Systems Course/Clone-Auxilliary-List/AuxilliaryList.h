//
// Created by Ajay Raj Nelapudi on 2019-05-25.
//

#ifndef CLONE_AUXILLIARY_LIST_AUXILLIARYLIST_H
#define CLONE_AUXILLIARY_LIST_AUXILLIARYLIST_H

#include <stdio.h>

typedef struct Node {
    int data;
    struct Node *next;
    struct Node *random;
} Node;

void printList(Node *head);
Node *makeLinkedList();
Node *copyNode(Node *node);
int listLength(Node *head);

#endif //CLONE_AUXILLIARY_LIST_AUXILLIARYLIST_H
