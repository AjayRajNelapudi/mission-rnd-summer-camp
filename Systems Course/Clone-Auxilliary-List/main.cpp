#include <stdio.h>
#include <stdlib.h>
#include <unordered_map>
#include "AuxilliaryList.h"
#include "CloneAuxilliaryList.h"
using namespace std;


int main() {
    Node *sampeLinkedList = makeLinkedList();
    printList(sampeLinkedList);

    Node *newList = cloneUsingUnorderedMap(sampeLinkedList);
    printList(newList);

    newList = cloneAuxilliaryList(sampeLinkedList);
    printList(newList);

    printList(sampeLinkedList);

    return 0;
}