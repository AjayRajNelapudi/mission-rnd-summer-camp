//
// Created by Ajay Raj Nelapudi on 2019-05-25.
//

#include <stdlib.h>
#include "CloneAuxilliaryList.h"
#include "AuxilliaryList.h"
#include <unordered_map>

Node *attachNodesToRandomPointers(Node *newHead, Node *list, std::unordered_map<Node*, Node*> addressMap) {
    Node *current = list;
    Node *newList = newHead;

    while (current != NULL) {
        Node *randomAddress = NULL;
        if (current->random != NULL) {
            randomAddress = addressMap[current->random];
        }

        newList->random = randomAddress;
        current = current->next;
        newList = newList->next;
    }

    return newHead;
}

Node *cloneUsingUnorderedMap(Node *list) {
    if (list == NULL) {
        return NULL;
    }

    std::unordered_map<Node *, Node *> addressMap;
    Node *newList = NULL, *newHead = NULL;
    Node *current = list;
    while (current != NULL) {
        if (newList == NULL) {
            newList = copyNode(current);
            newHead = newList;
            addressMap[current] = newList;
        } else {
            newList->next = copyNode(current);
            newList = newList->next;
            addressMap[current] = newList;
        }
        current = current->next;
    }

    current = list;
    newList = attachNodesToRandomPointers(newHead, list, addressMap);

    return newHead;
}

Node **getAuxilliaryListAsArray(Node *AuxilliaryList) {
    int len = listLength(AuxilliaryList);
    Node **auxiliaryListArray = (Node **)malloc(sizeof(Node *) * len);
    for (int i = 0; i < len; i++) {
        auxiliaryListArray[i] = AuxilliaryList;
        AuxilliaryList = AuxilliaryList->next;
    }

    return auxiliaryListArray;
}

void attachRandomPointers(Node **auxilliaryListArray, Node **newListArray, int n) {
    for (int i = 0; i < n; i++) {
        if (auxilliaryListArray[i]->random != NULL) {
            auxilliaryListArray[i]->next->random = auxilliaryListArray[i]->random->next;
        }
    }

    Node *prev = newListArray[0];
    for (int i = 1; i < n; i++) {
        prev->next = newListArray[i];
        prev = newListArray[i];
    }
}

Node *cloneAuxilliaryList(Node *list) {
    int len = listLength(list);
    Node** auxilliaryListArray = getAuxilliaryListAsArray(list);

    Node **newListArray = (Node **)malloc(sizeof(Node *) * len);
    for (int i = 0; i < len; i++) {
        newListArray[i] = copyNode(auxilliaryListArray[i]);
    }

    Node *prev = auxilliaryListArray[0];
    for (int i = 1; i < len; i++) {
        prev->next = newListArray[i - 1];
        newListArray[i - 1]->next = auxilliaryListArray[i];
        prev = auxilliaryListArray[i];
    }
    prev->next = newListArray[len - 1];

    //printList(*auxilliaryListArray);
    attachRandomPointers(auxilliaryListArray, newListArray, len);

    list = auxilliaryListArray[0];
    for (int i = 1; i < len; i++) {
        list->next = auxilliaryListArray[i];
        list = auxilliaryListArray[i];
    }
    list->next = NULL;

    return *newListArray;
}

