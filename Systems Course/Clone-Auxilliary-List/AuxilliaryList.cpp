//
// Created by Ajay Raj Nelapudi on 2019-05-25.
//

#include "AuxilliaryList.h"
#include <stdlib.h>

void printList(Node *head) {
    while (head != NULL) {
        int headRandomData = 0;
        if (head->random != NULL) {
            headRandomData = head->random->data;
        }
        printf("(%d, %d), ", head->data, headRandomData);
        head = head->next;
    }
    printf("\n");
}

Node *makeLinkedList() {
    const int size = 6;

    Node **nodes = (Node **)malloc(sizeof(Node *) * size);
    for (int i = 0; i < size; i++) {
        nodes[i] = (Node *)malloc(sizeof(Node));
        nodes[i]->data = i + 1;
        nodes[i]->next = NULL;
        nodes[i]->random = NULL;
    }

    nodes[0]->next = nodes[1];
    nodes[0]->random = nodes[2];

    nodes[1]->next = nodes[2];
    nodes[1]->random = nodes[4];

    nodes[2]->next = nodes[3];

    nodes[3]->next = nodes[4];
    nodes[3]->random = nodes[4];

    nodes[4]->next = nodes[5];

    nodes[5]->random = nodes[3];

    return *nodes;
}

Node *copyNode(Node *node) {
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->data = node->data;
    newNode->next = NULL;
    newNode->random = NULL;

    return newNode;
}

int listLength(Node *head) {
    int len = 0;

    while (head != NULL) {
        len++;
        head = head->next;
    }

    return len;
}