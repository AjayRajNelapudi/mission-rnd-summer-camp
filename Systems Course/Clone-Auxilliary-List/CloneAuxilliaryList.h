//
// Created by Ajay Raj Nelapudi on 2019-05-25.
//

#ifndef CLONE_AUXILLIARY_LIST_CLONEAUXILLIARYLIST_H
#define CLONE_AUXILLIARY_LIST_CLONEAUXILLIARYLIST_H

#include <stdio.h>
#include "AuxilliaryList.h"

Node *cloneUsingUnorderedMap(Node *list);
Node *cloneAuxilliaryList(Node *list);

#endif //CLONE_AUXILLIARY_LIST_CLONEAUXILLIARYLIST_H
