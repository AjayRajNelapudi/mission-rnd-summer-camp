#include <stdio.h>
#include <stdlib.h>
#include "LinkedList.h"

Node *connectLinkedLists(Node *LL1, Node *LL2) {
    if (LL1 == NULL) {
        return LL2;
    }

    if (LL2 == NULL) {
        return LL1;
    }

    Node *lastNode = LL1;
    while (lastNode->next != NULL) {
        lastNode = lastNode->next;
    }
    lastNode->next = LL2;

    return LL1;
}

Node *reverseKElements(Node *head, int k) {
    if (head == NULL) {
        return NULL;
    }

    if (k == 0) {
        return head;
    }

    Node *reversedList = NULL;
    Node *current = head;
    int subpartsCount = listLen(head) / k;
    for (int i = 0; i < subpartsCount; i++) {
        Node *newHead = current;
        Node *prev = NULL;
        for (int j = 0; j < k; j++) {
            prev = current;
            current = current->next;
        }
        prev->next = NULL;
        reversedList = connectLinkedLists(reversedList, reverseLinkedList(newHead));
    }

    return connectLinkedLists(reversedList, current);
}

int main() {
    Node *head = NULL;
    for (int i = 11; i <= 20; i++) {
        head = append(head, i);
    }

    head = reverseKElements(head, 3);
    printList(head);

    return 0;
}
