//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#ifndef REVERSE_K_NODES_ATTACH_LINKEDLIST_H
#define REVERSE_K_NODES_ATTACH_LINKEDLIST_H

typedef struct LinkedList {
    int data;
    struct LinkedList *next;
} Node;

Node *append(Node *head, int data);
void freeLinkedList(Node *head);
Node *constructLinkedListFromArray(int *elements, int n);
Node *constructLinkedListWithLoop(int *elements, int n, int loopBeginPosition);
Node *reverseLinkedList(Node *head);
int listLen(Node *head);
void printList(Node *head);

#endif //REVERSE_K_NODES_ATTACH_LINKEDLIST_H
