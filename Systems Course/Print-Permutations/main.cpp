#include <stdio.h>
#include <stdlib.h>

void printArray(int *arr, int *indices, int n) {
    if (arr == NULL) {
        return;
    }

    for (int i = 0; i < n; i++) {
        printf("%d", arr[indices[i]]);
    }

    printf("\n");
}

void printPermutations(int *elements, int n, int *indices, int index) {
    if (index >= n) {
        printArray(elements, indices, n);
        return;
    }

    for (int i = index; i < n; i++) {
        indices[index] = i;
        printPermutations(elements, n, indices, index + 1);
    }
}

int main() {
    int elements[] = {1, 3, 5};
    int size = sizeof(elements) / sizeof(elements[0]);
    int *indices = (int *)malloc(sizeof(int) * size);
    printPermutations(elements, size, indices, 0);

    return 0;
}