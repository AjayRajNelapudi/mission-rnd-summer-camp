#include <stdio.h>
#include <stdlib.h>

int **transposeMatrix(int **matrix, int m, int n) {
    int **result = (int **)malloc(sizeof(int *) * n);
    for (int i = 0; i < n; i++) {
        result[i] = (int *)malloc(sizeof(int) * m);
    }

    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            result[j][i] = matrix[i][j];
        }
    }

    return result;
}

void printMatrix(int **matrix, int m, int n) {
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

int main() {
    int **matrix = (int **)malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        matrix[i] = (int *)malloc(sizeof(int) * 4);
        for (int j = 0; j < 4; j++) {
            matrix[i][j] = i * 3 + j;
        }
    }
    int **result = transposeMatrix(matrix, 3, 4);

    printMatrix(matrix, 3, 4);
    printMatrix(result, 4, 3);
    return 0;
}
