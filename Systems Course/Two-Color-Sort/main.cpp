#include <stdio.h>
#include "TwoColorSort.h"

void printArray(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
}

int main() {
    int arr[] = {0, 0, 1, 1, 0, 1, 1, 0};
    int len = sizeof(arr) / sizeof(arr[0]);
    twoColorSortByTwoPointers(arr, len);
    printArray(arr, len);

    return 0;
}