//
// Created by Ajay Raj Nelapudi on 2019-05-25.
//

#ifndef TWO_COLOR_SORT_TWOCOLORSORT_H
#define TWO_COLOR_SORT_TWOCOLORSORT_H

void twoColorSortBySum(int *arr, int n);
void twoColorSortByTwoPointers(int *arr, int n);
void twoColorSortByBubbleSort(int *arr, int n);

#endif //TWO_COLOR_SORT_TWOCOLORSORT_H
