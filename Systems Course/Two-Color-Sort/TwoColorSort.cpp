//
// Created by Ajay Raj Nelapudi on 2019-05-25.
//

#include "TwoColorSort.h"

void twoColorSortBySum(int *arr, int n) {
    int ones = 0;
    for (int i = 0; i < n; i++) {
        ones += arr[i];
    }

    for (int i = 0; i < n - ones; i++) {
        arr[i] = 0;
    }

    for (int i = n - ones; i < n; i++) {
        arr[i] = 1;
    }
}

void twoColorSortByTwoPointers(int *arr, int n) {
    int begin = 0;
    int end = n - 1;

    while (begin < end) {
        if (arr[begin] == 0) {
            begin++;
        }

        if (arr[end] == 1) {
            end--;
        }

        if (arr[begin] == 1 && arr[end] == 0) {
            arr[begin] = 0;
            arr[end] = 1;
            begin++; end--;
        }
    }
}

void swap(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void twoColorSortByBubbleSort(int *arr, int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(&arr[j], &arr[j + 1]);
            }
        }
    }
}