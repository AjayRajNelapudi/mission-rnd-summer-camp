//
// Created by Ajay Raj Nelapudi on 2019-05-27.
//

#include "excel.h"
#include "utilities.h"
#include "expression.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void reset(Worksheet *worksheet) {
    if (worksheet == NULL) {
        return;
    }

    for (int i = 0; i < worksheet->rows; i++) {
        for (int j = 0; j < worksheet->columns; j++) {
            worksheet->cells[i][j].value = 0;
        }
    }
}

Worksheet* createWorksheet(int rows, int columns) {
    Worksheet* worksheet = (Worksheet *)malloc(sizeof(Worksheet));

    worksheet->rows = rows;
    worksheet->columns = columns;
    worksheet->cells = (Cell **)malloc(sizeof(Cell *) * rows);
    for (int i = 0; i < rows; i++) {
        worksheet->cells[i] = (Cell *)malloc(columns * sizeof(Cell));
        for (int j = 0; j < columns; j++) {
            worksheet->cells[i][j].value = 0;
            worksheet->cells[i][j].formula = (char *)malloc(sizeof(char) * 20);
        }
    }

    return worksheet;
}

void setValue(Worksheet *worksheet, int row, int column, int data) {
    if (worksheet == NULL) {
        return;
    }

    if (row < 0 || row > worksheet->rows) {
        return;
    }

    if (column < 0 || column > worksheet->columns) {
        return;
    }

    worksheet->cells[row][column].value = data;
}

void setFormula(Worksheet *worksheet, int row, int column, char *formula) {
    if (worksheet == NULL) {
        return;
    }

    if (row < 0 || row > worksheet->rows) {
        return;
    }

    if (column < 0 || column > worksheet->columns) {
        return;
    }

    strcpy(worksheet->cells[row][column].formula, formula);
}

void resetFormula(Worksheet *worksheet, int row, int column) {
    if (worksheet == NULL) {
        return;
    }

    if (row < 0 || row > worksheet->rows) {
        return;
    }

    if (column < 0 || column > worksheet->columns) {
        return;
    }

    worksheet->cells[row][column].formula[0] = '\0';
}

int get(Worksheet *worksheet, int row, int column, bool **visited) {
    if (worksheet == NULL) {
        return -1;
    }

    if (row < 0 || row > worksheet->rows) {
        return -1;
    }

    if (column < 0 || column > worksheet->columns) {
        return -1;
    }

    if (visited[row][column]) {
        return -1;
    }

    visited[row][column] = true;
    if (worksheet->cells[row][column].formula[0] == '\0') {
        return worksheet->cells[row][column].value;
    }

    //printf("%s\n", worksheet->cells[row][column].formula);
    int value = evaluateExpression(worksheet, worksheet->cells[row][column].formula, visited);
    if (value != -1) {
        worksheet->cells[row][column].value = value;
    }

    return value;
}

void print(Worksheet *worksheet) {
    if (worksheet == NULL) {
        return;
    }

    const int lineLength = worksheet->columns * 8 + 4;

    printLine(lineLength);
    printf("\t|");
    for (int i = 0; i < worksheet->columns; i++) {
        printf("%c\t\t|", 65 + i);
    }
    printf("\n");


    printLine(lineLength);
    for (int i = 0; i < worksheet->rows; i++) {
        printf("%d.\t|", i + 1);
        for (int j = 0; j < worksheet->columns; j++) {
            bool **visited = (bool **)calloc(worksheet->rows, sizeof(bool *));
            for (int i = 0; i < worksheet->rows; i++) {
                visited[i] = (bool *)calloc(worksheet->rows, sizeof(bool));
            }

            int value = get(worksheet, i, j, visited);
            if (value < 100) {
                printf("%d\t\t|", value);
            } else {
                printf("%d\t|", value);
            }
        }
        printf("\n");
        printLine(lineLength);
    }
}

void importFile(Worksheet *worksheet, FILE *csvFile) {
    if (worksheet == NULL || csvFile == NULL) {
        return;
    }

    reset(worksheet);

    int row = 0;
    int column = 0;
    char *buffer = (char *)calloc(worksheet->columns * 7, sizeof(char));
    while (fscanf(csvFile, "%s", buffer) && row < worksheet->rows) {
        char *token = NULL;

        while ((token = strtok_r(buffer, ",", &buffer)) && column < worksheet->columns) {
            worksheet->cells[row][column].value = atoi(token);
            column++;
        }
        row++;
        column = 0;
        free(buffer);
        buffer = (char *)calloc(worksheet->columns * 7, sizeof(char));
    }
}

void exportToFile(Worksheet *worksheet, FILE *csvFile) {
    if (worksheet == NULL || csvFile == NULL) {
        return;
    }

    for (int i = 0; i < worksheet->rows; i++) {
        for (int j = 0; j < worksheet->columns; j++) {
            bool **visited = (bool **)calloc(worksheet->rows, sizeof(bool *));
            for (int i = 0; i < worksheet->rows; i++) {
                visited[i] = (bool *)calloc(worksheet->rows, sizeof(bool));
            }

            int value = get(worksheet, i, j, visited);
            fprintf(csvFile, "%d,", value);
        }
        fprintf(csvFile, "\n");
    }
}