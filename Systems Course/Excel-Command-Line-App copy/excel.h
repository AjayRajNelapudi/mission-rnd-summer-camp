//
// Created by Ajay Raj Nelapudi on 2019-05-27.
//

#ifndef EXCEL_COMMAND_LINE_APP_EXCEL_H
#define EXCEL_COMMAND_LINE_APP_EXCEL_H

#include <stdio.h>

typedef struct Cell {
    int value;
    char *formula;
} Cell;

typedef struct Worksheet {
    int rows;
    int columns;
    Cell **cells;
    char *importFile;
} Worksheet;

Worksheet* createWorksheet(int rows, int columns);
void setValue(Worksheet *worksheet, int row, int column, int data);
void setFormula(Worksheet *worksheet, int row, int column, char *formula);
void resetFormula(Worksheet *worksheet, int row, int column);
int get(Worksheet *worksheet, int row, int column, bool **visited);
void print(Worksheet *worksheet);
void importFile(Worksheet *worksheet, FILE *csvFile);
void exportToFile(Worksheet *worksheet, FILE *csvFile);

#endif //EXCEL_COMMAND_LINE_APP_EXCEL_H
