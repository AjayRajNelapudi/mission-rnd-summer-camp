//
// Created by Ajay Raj Nelapudi on 2019-05-28.
//

#include "utilities.h"
#include "expression.h"
#include "stack.h"
#include "excel.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

int precedenceOf(char operatorSymbol) {
    switch(operatorSymbol) {
        case '^': return 1;
        case '/': return 2;
        case '%': return 2;
        case '*': return 2;
        case '+': return 3;
        case '-': return 3;
        case -1: return 4;
        default: return 4;
    }
}

Expression* parseExpression(char *expression) {
    if (!isValidString(expression)) {
        return NULL;
    }

    int i = 0;
    int len = strlen(expression);
    Expression *infix = NULL;

    while (i < len) {
        if (expression[i] == '(' || expression[i] == ')' || isOperator(expression[i])) {
            infix = append(infix, toString(expression[i]));
            i++;
        } else if (isspace(expression[i])) {
            i++;
        } else {
            int index = 0;
            char *operand = (char *)malloc(sizeof(char) * 20);
            while (isalnum(expression[i])) {
                operand[index++] = expression[i++];
            }
            operand[index] = '\0';
            infix = append(infix, operand);
        }
    }

    return infix;
}

Expression* infixToPostfix(Expression *infix) {
    if (infix == NULL) {
        return NULL;
    }

    Expression *postfix = NULL;
    Stack *top = NULL;

    while (infix != NULL) {
        char *term = infix->term;
        int len = strlen(term);

        if (len == 1) {
            if (term[0] == '(') {
                top = push(top, term[0]);
            } else if (term[0] == ')') {
                while (!isEmpty(top) && peek(top) != '(') {
                    int symbol = peek(top);
                    top = pop(top);
                    postfix = append(postfix, toString(symbol));
                }
                top = pop(top);
            } else {
                while (!isEmpty(top) && precedenceOf(peek(top)) <= precedenceOf(term[0]) && peek(top) != '(') {
                   int operater = peek(top);
                   top = pop(top);
                   postfix = append(postfix, toString(operater));
                }
                top = push(top, term[0]);
            }
        } else {
            postfix = append(postfix, term);
        }

        infix = infix->next;
    }

    while (!isEmpty(top)) {
        int operater = peek(top);
        postfix = append(postfix, toString(operater));
        top = pop(top);
    }

    return postfix;
}

int postfixEvaluate(Expression *postfix, Worksheet *worksheet, bool **visited) {
    if (postfix == NULL) {
        return -1;
    }

    if (visited == NULL) {
        return -1;
    }

    if (worksheet == NULL) {
        return -1;
    }

    Stack *top = NULL;

    while (postfix != NULL) {
        if (strlen(postfix->term) == 1) {
            int operandA = peek(top);
            top = pop(top);
            int operandB = peek(top);
            top = pop(top);

            int result = compute(operandA, operandB, postfix->term[0]);
            top = push(top, result);
        } else {
            char *address = (char *)malloc(sizeof(char) * 20);
            strcpy(address, postfix->term);
            int *indices = getRowAndColumn(address);

            int value = get(worksheet, indices[0], indices[1], visited);
            if (value == -1 && isNumber(address)) {
                value = atoi(address);
            }
            top = push(top, value);
        }
        postfix = postfix->next;
    }

    return peek(top);
}

int evaluateExpression(Worksheet *worksheet, char *expression, bool **visited) {
    Expression *infix = parseExpression(expression);
    //printList(infix);
    Expression *postfix = infixToPostfix(infix);
    //printList(postfix);
    return postfixEvaluate(postfix, worksheet, visited);
}