//
// Created by Ajay Raj Nelapudi on 2019-05-28.
//

#ifndef EXCEL_COMMAND_LINE_APP_EXPRESSION_H
#define EXCEL_COMMAND_LINE_APP_EXPRESSION_H

#include "excel.h"

int infixToPostfix(char *infix, char *postfix);
int evaluateExpression(Worksheet *worksheet, char *expression, bool **visited);

#endif //EXCEL_COMMAND_LINE_APP_EXPRESSION_H
