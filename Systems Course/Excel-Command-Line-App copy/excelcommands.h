//
// Created by Ajay Raj Nelapudi on 2019-05-27.
//

#ifndef EXCEL_COMMAND_LINE_APP_EXCELCOMMANDS_H
#define EXCEL_COMMAND_LINE_APP_EXCELCOMMANDS_H

void setCommand(Worksheet *worksheet, char *line, int lineLength);
void getCommand(Worksheet *worksheet, char *line, int lineLength);
void importCommand(Worksheet *worksheet, char *line, int lineLength);
void exportCommand(Worksheet *worksheet, char *line, int lineLength);
void saveCommand(Worksheet *worksheet);

#endif //EXCEL_COMMAND_LINE_APP_EXCELCOMMANDS_H
