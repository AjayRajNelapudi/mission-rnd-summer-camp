//
// Created by Ajay Raj Nelapudi on 2019-05-27.
//

#ifndef EXCEL_COMMAND_LINE_APP_UTILITIES_H
#define EXCEL_COMMAND_LINE_APP_UTILITIES_H

typedef struct Expression {
    char *term;
    struct Expression *next;
} Expression;

bool isValidString(char *string);
int* getRowAndColumn(char *cell);
void readCellAndValue(char *line, int lineLength, char *cell, char *value);
void separateRowAndColumn(char *cell, char *row, char *column);
void getFilename(char *line, char *filename);
int fetchToken(char *str, char *word);
int nextChar(char *str);
int countOperators(char *expression);
int compute(int valueA, int valueB, char operation);
void printLine(int length);
void addExtension(char *filename);
void strlower(char *s);
bool stringMatchN(char *line, char *key, int len);
void popFront(char *command, int offset);
char *readCommand();
bool isNumber(char *string);
Expression* append(Expression *expression, char *term);
char* toString(char ch);
void printList(Expression *expr);
bool isOperator(char op);

#endif //EXCEL_COMMAND_LINE_APP_UTILITIES_H
