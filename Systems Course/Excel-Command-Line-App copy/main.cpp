#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "excel.h"
#include "excelcommands.h"
#include "utilities.h"

int main() {
    const int rows = 10;
    const int cols = 10;

    Worksheet *worksheet = createWorksheet(rows, cols);
    int exitFlag = 0;

    char *command = NULL;
    while (!exitFlag) {
        printf("> ");
        command = readCommand();

        if (stringMatchN(command, "SET", 3)) {
            popFront(command, 3);
            setCommand(worksheet, command, strlen(command));
        } else if (stringMatchN(command, "GET", 3)) {
            popFront(command, 3);
            getCommand(worksheet, command, strlen(command));
        } else if (stringMatchN(command, "PRINT", 5)) {
            print(worksheet);
        } else if (stringMatchN(command, "IMPORT", 6)) {
            popFront(command, 6);
            importCommand(worksheet, command, strlen(command));
        } else if (stringMatchN(command, "EXPORT", 6)) {
            popFront(command, 6);
            exportCommand(worksheet, command, strlen(command));
        } else if (stringMatchN(command, "SAVE", 4)) {
            popFront(command, 4);
            saveCommand(worksheet);
        } else if (stringMatchN(command, "EXIT", 4)) {
            exitFlag = 1;
        } else {
            printf("command does not exist\n");
        }
    }

    return 0;
}
