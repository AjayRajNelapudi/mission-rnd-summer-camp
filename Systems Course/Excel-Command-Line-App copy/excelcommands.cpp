//
// Created by Ajay Raj Nelapudi on 2019-05-27.
//

#include "excel.h"
#include "excelcommands.h"
#include "utilities.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void setCommand(Worksheet *worksheet, char *line, int lineLength) {
    if (worksheet == NULL) {
        return;
    }

    if (!isValidString(line)) {
        return;
    }

    int operators = countOperators(line);

    char *cell = (char *)malloc(100 * sizeof(char));
    char *value = (char *)malloc(100 * sizeof(char));
    int *indices = NULL;

    bool **visited = (bool **)calloc(worksheet->rows, sizeof(bool *));
    for (int i = 0; i < worksheet->rows; i++) {
        visited[i] = (bool *)calloc(worksheet->rows, sizeof(bool));
    }

    int integerValue = -1;

    switch(operators) {
        case 0:
            sscanf(line, "%s = %s", cell, value);
            indices = getRowAndColumn(cell);

            integerValue = atoi(value);
            if (integerValue == 0 && !isNumber(value)) {
                setFormula(worksheet, indices[0], indices[1], value);
                if (get(worksheet, indices[0], indices[1], visited) == -1) {
                    printf("Invalid Formula\n");
                    resetFormula(worksheet, indices[0], indices[1]);
                }
            } else {
                setValue(worksheet, indices[0], indices[1], integerValue);
            }
            break;

        default:
            sscanf(line, "%s = %[^\n]s", cell, value);
            indices = getRowAndColumn(cell);

            setFormula(worksheet, indices[0], indices[1], value);
            if (get(worksheet, indices[0], indices[1], visited) == -1) {
                printf("Invalid Formula\n");
                resetFormula(worksheet, indices[0], indices[1]);
            }
    }
}

void getCommand(Worksheet *worksheet, char *line, int lineLength) {
    if (worksheet == NULL) {
        return;
    }

    if (!isValidString(line)) {
        return;
    }

    int *indices = getRowAndColumn(line);

    bool **visited = (bool **)calloc(worksheet->rows, sizeof(bool *));
    for (int i = 0; i < worksheet->rows; i++) {
        visited[i] = (bool *)calloc(worksheet->rows, sizeof(bool));
    }

    int value = get(worksheet, indices[0], indices[1], visited);
    printf("%d\n", value);
}

void importCommand(Worksheet *worksheet, char *line, int lineLength) {
    if (worksheet == NULL) {
        return;
    }

    if (!isValidString(line)) {
        return;
    }

    char *filename = (char *)malloc(20 * sizeof(char));
    getFilename(line, filename);

    FILE *csvFile = fopen(filename, "r");
    if (csvFile == NULL) {
        printf("%s does not exist\n", filename);
        fclose(csvFile);
        return;
    }

    importFile(worksheet, csvFile);
    worksheet->importFile = (char *)malloc(sizeof(char) * 50);
    strcpy(worksheet->importFile, filename);
    fclose(csvFile);
}

void exportCommand(Worksheet *worksheet, char *line, int lineLength) {
    if (worksheet == NULL) {
        return;
    }

    if (!isValidString(line)) {
        return;
    }

    char *filename = (char *)malloc(20 * sizeof(char));
    getFilename(line, filename);
    addExtension(filename);

    FILE *csvFile = fopen(filename, "r");
    if (csvFile != NULL) {
        printf("%s already exists. Do you want to overwrite? [y / n] ");
        char choice;
        scanf("%c", &choice);
        if (choice != 'Y' && choice != 'y') {
            return;
        }
    }
    fclose(csvFile);

    csvFile = fopen(filename, "w");
    exportToFile(worksheet, csvFile);
    fclose(csvFile);
}

void saveCommand(Worksheet *worksheet) {
    if (worksheet == NULL) {
        return;
    }

    if (!isValidString(worksheet->importFile)) {
        printf("Excel sheet not imported.\n");
        return;
    }

    FILE *import = fopen(worksheet->importFile, "w");
    exportToFile(worksheet, import);
    fclose(import);
}