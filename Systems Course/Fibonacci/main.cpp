#include <stdio.h>
#include <unordered_map>
using namespace std;

long fibonacci(long n, unordered_map <long, long> fibonacciValues) {
    if (n < 0) {
        return 0;
    }

    if (n == 0 || n == 1) {
        return n;
    }

    if (fibonacciValues.find(n) == fibonacciValues.end()) {
        fibonacciValues[n] = fibonacci(n - 1, fibonacciValues) + fibonacci(n - 2, fibonacciValues);
    }
    return fibonacciValues[n];
}

int main() {
    unordered_map <long, long> fibonacciValues;
    int num = fibonacci(20, fibonacciValues);
    printf("%d\n", num);

    return 0;
}