//
// Created by Ajay Raj Nelapudi on 2019-05-31.
//

#ifndef FILE_SYSTEM_NON_CONTINUOUS_DISKIO_H
#define FILE_SYSTEM_NON_CONTINUOUS_DISKIO_H

#define MAGIC_NUMBER 0x444E524D
#define BLOCK_SIZE (16 * 1024)
#define METADATA_SIZE (BLOCK_SIZE * 1000)
#define NO_OF_BLOCKS 100000
#define DISK "/Users/ajayraj/Documents/MRND-Summer/File-System-Non-Continuous/HardDisk.hdd"

typedef struct FileRecord {
    char filename[20];
    unsigned int fileSize;
    unsigned int noOfBlocks;

} FileRecord;

typedef struct Files {
    unsigned int noOfFiles;
    FileRecord fileRecord[NO_OF_BLOCKS];
} Files;

typedef struct Metadata {
    unsigned int magicNumber;
    unsigned int blockSize;
    unsigned int noOfBlocks;
    unsigned int noOfEmptyBlocks;
    bool freeBlockList[NO_OF_BLOCKS];
} Metadata;

bool readBlock(char *buffer, unsigned int blockId);
bool writeBlock(char *buffer, unsigned int blockId);
Metadata *readMetadata();
void writeMetadata(Metadata *metadata);
unsigned int mount(char *diskName, unsigned int sizeOfBlock);
Files *readFilesData();
void writeFilesData(Files *filesData);

#endif //FILE_SYSTEM_NON_CONTINUOUS_DISKIO_H
