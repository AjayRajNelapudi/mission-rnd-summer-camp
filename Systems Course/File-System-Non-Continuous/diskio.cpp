//
// Created by Ajay Raj Nelapudi on 2019-05-31.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "diskio.h"
#include "utilities.h"

//static unsigned int BLOCK_SIZE;
//static char DISK[50];

bool readBlock(char *buffer, unsigned int blockId) {
    if (buffer == NULL) {
        return false;
    }

    FILE *disk = fopen(DISK, "rb");
    if (disk == NULL) {
        return false;
    }

    fseek(disk, blockId * BLOCK_SIZE, SEEK_SET);

    if (fread(buffer, sizeof(char), BLOCK_SIZE, disk) < 0) {
        return false;
    }
    fclose(disk);

    return true;
}

bool writeBlock(char *buffer, unsigned int blockId) {
    if (buffer == NULL) {
        return false;
    }

    FILE *disk = fopen(DISK, "rb+");
    if (disk == NULL) {
        return false;
    }

    fseek(disk, blockId * BLOCK_SIZE, SEEK_SET);

    if (fwrite(buffer, sizeof(char), BLOCK_SIZE, disk) < 0) {
        return false;
    }
    fclose(disk);

    return true;
}

Metadata *readMetadata() {
    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    Metadata *metadata = (Metadata *)malloc(sizeof(Metadata));

    readBlock(buffer, 0);
    memccpy(metadata, buffer, 1, sizeof(Metadata));
    free(buffer);

    return metadata;
}

void writeMetadata(Metadata *metadata) {
    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    memccpy(buffer, metadata, 1, sizeof(Metadata));

    writeBlock(buffer, 0);

    free(buffer);
}

Files *readFilesData() {
    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    Files *filesData = (Files *)malloc(sizeof(Files));

    readBlock(buffer, METADATA_SIZE / BLOCK_SIZE);
    memccpy(filesData, buffer, 1, sizeof(Files));
    free(buffer);

    return filesData;
}

void writeFilesData(Files *filesData) {
    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    memccpy(buffer, filesData, 1, sizeof(Files));

    writeBlock(buffer, METADATA_SIZE / BLOCK_SIZE);

    free(buffer);
}

unsigned int init(char *diskName, unsigned int sizeOfBlock) {
    if (diskName == NULL) {
        return -1;
    }

    //strcpy(DISK, diskName);
    BLOCK_SIZE = sizeOfBlock;
    unsigned int fileSize = getFileSize(DISK);
    unsigned int noOfBlocks = 0;

    Metadata *metadata = readMetadata();
    if (metadata->magicNumber != MAGIC_NUMBER) {
        metadata->blockSize = BLOCK_SIZE;
        metadata->noOfBlocks = fileSize / BLOCK_SIZE;
        noOfBlocks = metadata->noOfBlocks;
        metadata->noOfEmptyBlocks = metadata->noOfBlocks;
    }
    writeMetadata(metadata);
    free(metadata);

    return noOfBlocks;
}