//
// Created by Ajay Raj Nelapudi on 2019-05-31.
//

#ifndef FILE_SYSTEM_NON_CONTINUOUS_UTILITIES_H
#define FILE_SYSTEM_NON_CONTINUOUS_UTILITIES_H

unsigned int getFileSize(char *filename);
bool stringMatch(char *line, char *key);
char *readCommand();

#endif //FILE_SYSTEM_NON_CONTINUOUS_UTILITIES_H
