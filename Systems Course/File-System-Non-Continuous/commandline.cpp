//
// Created by Ajay Raj Nelapudi on 2019-05-31.
//

#include <stdio.h>
#include <stdlib.h>
#include "commandline.h"
#include "utilities.h"
#include "filehandler.h"

int parseCommandLine() {
    char *commandline = readCommand();

    char command[15], argument1[50], argument2[50];
    if (stringMatch(commandline, "MOUNT")) {
        sscanf(commandline, "%s %s %s", command, argument1, argument2);
        //mount(argument1, atoi(argument2));
    } else if (stringMatch(commandline, "COPYFROMDISK")) {
        sscanf(commandline, "%s %s %s", command, argument1, argument2);
        exportFile(argument1, argument2);
    } else if (stringMatch(commandline, "COPYTODISK")) {
        sscanf(commandline, "%s %s %s", command, argument1, argument2);
        importFile(argument1, argument2);
    } else if (stringMatch(commandline, "DEBUG")) {
        debug();
    } else if (stringMatch(commandline, "EXIT")) {
        return 1;
    }

    return 0;
}