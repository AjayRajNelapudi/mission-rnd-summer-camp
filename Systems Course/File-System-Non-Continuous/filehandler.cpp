//
// Created by Ajay Raj Nelapudi on 2019-05-31.
//

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include "filehandler.h"
#include "diskio.h"
#include "utilities.h"

void importFile(char *source, char *destination) {
    if (source == NULL || destination == NULL) {
        return;
    }

    FILE *sourceFile = fopen(source, "rb");
    if (sourceFile == NULL) {
        return;
    }

    int fileSize = getFileSize(source);
    if (fileSize > metadata->noOfEmptyBlocks * BLOCK_SIZE) {
        return;
    }

    char *buffer = (char *)malloc(sizeof(char) * BLOCK_SIZE);
    Metadata *metadata = readMetadata();
    Files *filesData = readFilesData();

    int noOfBlocks = ceil(fileSize / BLOCK_SIZE);
    if (noOfBlocks == 0) {
        noOfBlocks = fileSize;
    }
    int remainingFileSize = fileSize;

    for (int i = 0; i < metadata->noOfBlocks && remainingFileSize > 0; i++) {
        if (!metadata->freeBlockList[i]) {
            fread(buffer, BLOCK_SIZE, 1, sourceFile);
            writeBlock(buffer, i);
            metadata->freeBlockList[i] = true;
            remainingFileSize -= BLOCK_SIZE;
        }
    }

    int index = filesData->noOfFiles;
    strcpy(filesData->fileRecord[index].filename, destination);
    filesData->fileRecord[index].fileSize = fileSize;
    filesData->fileRecord[index].noOfBlocks = noOfBlocks;
    filesData->noOfFiles++;

    writeFilesData(filesData);
    free(filesData);
    metadata->noOfEmptyBlocks -= noOfBlocks;
    writeMetadata(metadata);
    free(filesData);
}