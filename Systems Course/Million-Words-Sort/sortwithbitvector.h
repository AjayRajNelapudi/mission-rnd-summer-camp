//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#ifndef MILLION_WORDS_SORT_SORTWITHBITVECTOR_H
#define MILLION_WORDS_SORT_SORTWITHBITVECTOR_H

#include <stdio.h>

void sortMillionWordsUsingBitVector(FILE *millionWordsFile, int range, int limit);

#endif //MILLION_WORDS_SORT_SORTWITHBITVECTOR_H
