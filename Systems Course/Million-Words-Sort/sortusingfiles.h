//
// Created by Ajay Raj Nelapudi on 2019-05-29.
//

#ifndef MILLION_WORDS_SORT_SORTUSINGFILES_H
#define MILLION_WORDS_SORT_SORTUSINGFILES_H
#include <stdio.h>

void sortMillionWords(FILE *millionWordsFile, int n, int limit);

#endif //MILLION_WORDS_SORT_SORTUSINGFILES_H
