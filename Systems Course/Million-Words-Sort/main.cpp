#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "sortwithbitvector.h"

int main() {
    int TOTAL_WORDS = 1000000;
    int MEMORY_LIMIT = 1000000;
    int RANGE_UPPERLIMIT = 10000000;
    char FILE_PATH[] = "/Users/ajayraj/Documents/MRND-Summer/Million-Words-Sort/input.txt";
    FILE *millionWordsFile = fopen(FILE_PATH, "r");
    //sortMillionWords(millionWordsFile, TOTAL_WORDS, MEMORY_LIMIT);
    sortMillionWordsUsingBitVector(millionWordsFile, RANGE_UPPERLIMIT, MEMORY_LIMIT);
    fclose(millionWordsFile);
    return 0;
}