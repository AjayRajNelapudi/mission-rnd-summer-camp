//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#include "sortwithbitvector.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void sortMillionWordsUsingBitVector(FILE *millionWordsFile, int range, int limit) {
    int passes = ceil(range / limit);
    int limitIsPresent = false;

    int number = 0;
    for (int i = 0; i < passes; i++) {
        bool *isPresent = (bool *)calloc(limit, sizeof(bool));
        int lowerLimit = i * limit;
        int upperLimit = (i + 1) * limit;
        for (int j = 0; j < limit; j++) {
            fscanf(millionWordsFile, "%d", &number);

            if (number == range) {
                limitIsPresent = true;
            }

            if (number >= lowerLimit && number < upperLimit) {
                int index = number - lowerLimit;
                isPresent[index] = true;
            }
        }

        for (int i = 0; i < limit; i++) {
            if (isPresent[i]) {
                printf("%d\n", lowerLimit + i);
            }
        }

        free(isPresent);
    }

    if (limitIsPresent) {
        printf("%d\n", range);
    }
}