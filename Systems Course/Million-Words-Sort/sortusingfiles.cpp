//
// Created by Ajay Raj Nelapudi on 2019-05-29.
//

#include "sortusingfiles.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>

int comparator(const void *a, const void *b) {
    return *(int *)a - *(int *)b;
}

char *generateFilename(int n) {
    char *filename = (char *)malloc(sizeof(char) * 100);
    strcpy(filename, "file");

    int i = 0;
    for (i = 0; i <= n; i++) {
        filename[4 + i] = '$';
    }
    strcpy(filename + 4 + i, ".txt");

    return filename;
}

void readFilesAndSort(int n) {
    FILE **files = (FILE **)malloc(sizeof(FILE *) * n);
    for (int i = 0; i < n; i++) {
        char *filename = generateFilename(i);
        files[i] = fopen(filename, "r");
        free(filename);
    }

    FILE *outputFile = fopen("output.txt", "w");

    char *number = (char *)malloc(sizeof(char) * 10);
    int num;
    bool exitFlag = false;

    while (!exitFlag) {
        exitFlag = true;
        int minValue = INT_MAX, minValueIndex = -1;
        for (int i = 0; i < n; i++) {
            if (fscanf(files[i], "%s", number) == EOF) {
                continue;
            }

            exitFlag = false;
            num = atoi(number);
            if (minValue > num) {
                minValue = num;
                minValueIndex = i;
            }
            fseek(files[i], strlen(number) * -1, SEEK_CUR);
        }

        if (!exitFlag) {
            fscanf(files[minValueIndex], "%s", number);
            fprintf(outputFile, "%d\n", atoi(number));
        }
    }


    free(number);
    for (int i = 0; i < n; i++) {
        fclose(files[i]);
    }
}

void sortMillionWords(FILE *millionWordsFile, int n, int limit) {
    if (millionWordsFile == NULL) {
        return;
    }

    int *arr = (int *)malloc(sizeof(int) * limit);
    int counter = 0;
    for (int i = 0; i < n / limit; i++) {
        for (int j = 0; j < limit; j++) {
            fscanf(millionWordsFile, "%d", &arr[j]);
        }
        qsort(arr, limit, sizeof(int), comparator);

        char *filename = generateFilename(i);
        FILE *partFile = fopen(filename, "w");
        for (int j = 0; j < limit; j++) {
            fprintf(partFile, "%d\n", arr[j]);
        }
        fclose(partFile);
        free(filename);

        counter = i;
    }

    int extra = 0;
    if (n % limit != 0) {
        extra = 1;
    }

    int i = 0;
    while (fscanf(millionWordsFile, "%d", &arr[i]) != EOF) {
        i++;
    }
    arr[i] = '\0';
    char *filename = generateFilename(counter);
    FILE *partFile = fopen(filename, "w");
    qsort(arr, i, sizeof(int), comparator);
    for (int j = 0; j < i; j++) {
        fprintf(partFile, "%d\n", arr[j]);
    }
    fclose(partFile);

    free(arr);
    free(filename);

    readFilesAndSort(counter + extra + 1);
}