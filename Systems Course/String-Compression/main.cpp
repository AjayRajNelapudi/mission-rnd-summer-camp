#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int convertAndAppend(char *str, int exponent, int count, int i) {
    if (str == NULL || *str == '\0') {
        return i;
    }

    if (count == 1) {
        return i;
    }

    while (exponent--) {
        int digit = count / pow(10, exponent);
        str[i++] = (char)digit + 48;
        count = (int)count % (int)pow(10, exponent);
    }

    return i;
}


void encodeRunLength(char *str) {
    if (str == NULL || *str == '\0') {
        return;
    }

    int i = 0, j = 0;
    char prevChar = str[0];
    int exponent = 1;

    char currentChar = str[0];
    int count = 0;

    while(str[j] != '\0') {
        if (str[j] == prevChar) {
            count++;

            if (count >= pow(10, exponent)) {
                exponent++;
            }
        } else if (count > 1) {
            str[i++] = currentChar;
            currentChar = str[j];

            i = convertAndAppend(str, exponent, count, i);
            count = 1;
            exponent = 1;
        } else {
            str[i++] = prevChar;
        }

        prevChar = str[j];
        j++;
    }

    if (count == 0) {
        str[i] = '\0';
        return;
    }

    str[i++] = prevChar;
    i = convertAndAppend(str, exponent, count, i);
    str[i] = '\0';
}


int main() {
    //char s[] = "aaaaaaaaaaabbbbbbbc";
    //char s[] = "abcdef";
    //char s[] = "aabbccddefg";
    char s[] = "aaaaabbbbbbaaaabbbbwwwwddddddppppaaxxxxzzzzabcde";
    encodeRunLength(s);
    printf("%s\n", s);

    return 0;
}