//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#ifndef SEARCH_CROSSWORD_SEARCHCROSSWORD_H
#define SEARCH_CROSSWORD_SEARCHCROSSWORD_H

typedef struct Point {
    int x;
    int y;
} Point;

typedef struct EndPoint {
    Point *startPoint;
    Point *endPoint;
} EndPoint;

EndPoint **searchInCrossword(char **crossword, int m, int n, char *key, int keyLength, int *len);

#endif //SEARCH_CROSSWORD_SEARCHCROSSWORD_H
