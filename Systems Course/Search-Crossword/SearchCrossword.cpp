//
// Created by Ajay Raj Nelapudi on 2019-05-26.
//

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "SearchCrossword.h"

Point *makePoint(int x, int y) {
    Point *newPoint = (Point *)malloc(sizeof(Point));
    newPoint->x = x;
    newPoint->y = y;

    return newPoint;
}

Point *searchRecursively(char **crossword, int m, int n, int x, int y, char *key, int keyLength, Point *applyToPoint) {
    if (key == NULL) {
        return NULL;
    }

    if (x < 0 || x >= m || y < 0 || y >= n) {
        return NULL;
    }

    if (crossword == NULL) {
        return NULL;
    }

    if (*key != crossword[x][y]) {
        return NULL;
    } else if (keyLength == 1) {
        return makePoint(x, y);
    }

    x += applyToPoint->x;
    y += applyToPoint->y;

    return searchRecursively(crossword, m, n, x, y, key + 1, keyLength - 1, applyToPoint);
}

EndPoint **searchInCrossword(char **crossword, int m, int n, char *key, int keyLength, int *len) {
    EndPoint **occurences = (EndPoint **)malloc(sizeof(EndPoint) * m * n);
    int occurencesIndex = 0;

    int directions[][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, 1}, {-1, -1}, {1, -1}, {-1, 1}};
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < sizeof(directions) / sizeof(directions[0]); k++) {
                Point *applyToPoint = makePoint(directions[k][0], directions[k][1]);
                Point *endPoint = searchRecursively(crossword, m, n, i, j, key, keyLength, applyToPoint);

                if (endPoint != NULL) {
                    occurences[occurencesIndex] = (EndPoint *)malloc(sizeof(EndPoint));
                    occurences[occurencesIndex]->startPoint = makePoint(i, j);
                    occurences[occurencesIndex]->endPoint = endPoint;
                    occurencesIndex++;
                }

                if (keyLength == 1) {
                    break;
                }
            }
        }
    }

    *len = occurencesIndex;
    return occurences;
}