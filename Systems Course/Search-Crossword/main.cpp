#include <stdio.h>
#include "SearchCrossword.h"

int main() {
    //char *crossword[5] = {"HiHiH", "eeHee"};
    char *crossword[4] = {"eeee", "eeee", "eeee", "eeee"};
    char key[] = "e";
    int keyLength = sizeof(key) / sizeof(key[0]) - 1;
    int len = 0;

    EndPoint **occurences = searchInCrossword(crossword, 4, 4, key, keyLength, &len);
    for (int i = 0; i < len; i++) {
        printf("(%d %d) (%d %d)", occurences[i]->startPoint->x,
                                  occurences[i]->startPoint->y,
                                  occurences[i]->endPoint->x,
                                  occurences[i]->endPoint->y);
        printf("\n");
    }
    printf("Total Occurences: %d\n", len);

    return 0;
}