//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#ifndef FILE_SYSTEM_DISKUTILITIES_H
#define FILE_SYSTEM_DISKUTILITIES_H
#include "metadata.h"
#define DISK "/Users/ajayraj/Documents/MRND-Summer/File-System/HardDisk.hdd"

void listFiles(Metadata *metadata, int n);
Metadata *formatDisk(Metadata *metadata);
Metadata *deleteFile(Metadata *metadata, char *filename);
Metadata *readTableFromDisk();
void writeTableToDisk(Metadata *metadata);
Metadata *importFile(Metadata *metadata, char *source, char *destination);
Metadata *exportFile(Metadata *metadata, char *source, char *destination);
unsigned int readNFromDisk();
void writeNToDisk(unsigned int n);

#endif //FILE_SYSTEM_DISKUTILITIES_H
