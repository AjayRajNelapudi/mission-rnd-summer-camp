#include "diskutilities.h"
#include "metadata.h"
#include "helper.h"
#include <stdio.h>
#include <stdlib.h>

int main() {
    int n = readNFromDisk();
    Metadata *metadata = readTableFromDisk();

    while (1) {
        printf("> ");
        char *command = readCommand();

        if (stringMatchN(command, "exit", 4)) {
            break;
        } else if (stringMatchN(command, "format", 6) || stringMatchN(command, "init", 4 )) {
            n = 0;
            metadata = formatDisk(metadata);
        } else if (stringMatchN(command, "delete", 6)) {
            command = command + 7;
            metadata = deleteFile(metadata, command);
            n -= 1;
        } else if (stringMatchN(command, "import", 6)) {
            char src[30], dest[30];
            command = command + 7;
            sscanf(command, "%s %s", src, dest);
            metadata = importFile(metadata, src, dest);
            n += 1;
        } else if (stringMatchN(command, "export", 6)) {
            char src[30], dest[30];
            command = command + 7;
            sscanf(command, "%s %s", src, dest);
            metadata = exportFile(metadata, src, dest);
        } else if (stringMatchN(command, "ls", 2)) {
            listFiles(metadata, n);
        } else {
            printf("Command Not Found.\n");
        }
    }

    writeNToDisk(n);
    writeTableToDisk(metadata);
    return 0;
}