//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#include "helper.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char *getName(char *string) {
    char *namebBegin = string;

    while(*string != '\0') {
        if (*string == '/') {
            namebBegin = string + 1;
        }
        string++;
    }

    return namebBegin;
}

void strlower(char *s) {
    int c = 0;

    while (s[c] != '\0') {
        if (s[c] >= 'A' && s[c] <= 'Z') {
            s[c] = s[c] + 32;
        }
        c++;
    }
}

bool stringMatchN(char *line, char *key, int len) {
    if (strlen(line) < len) {
        return false;
    }

    if (strlen(key) < len) {
        return false;
    }

    char *commandCopy = (char *)malloc((len + 1) * sizeof(char));
    strcpy(commandCopy, line);
    commandCopy[len] = '\0';

    char *keyCopy = (char *)malloc((len + 1) * sizeof(char));
    strcpy(keyCopy, key);
    keyCopy[len] = '\0';

    strlower(commandCopy);
    strlower(keyCopy);

    if (strcmp(commandCopy, keyCopy) == 0) {
        return true;
    }

    return false;
}

char *readCommand() {
    char *command = (char *)malloc(100 * sizeof(char));
    scanf("%[^\n]s", command);
    char extra;
    scanf("%c", &extra);

    return command;
}