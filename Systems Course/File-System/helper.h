//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#ifndef FILE_SYSTEM_HELPER_H
#define FILE_SYSTEM_HELPER_H

char *getName(char *string);
bool stringMatchN(char *line, char *key, int len);
char *readCommand();

#endif //FILE_SYSTEM_HELPER_H
