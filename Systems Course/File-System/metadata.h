//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#ifndef FILE_SYSTEM_METADATA_H
#define FILE_SYSTEM_METADATA_H

typedef struct MetadataTable {
    unsigned int offset;
    unsigned int length;
    char filename[16];
    struct MetadataTable *next;
} Metadata;

Metadata *createMetadata(unsigned int offset, unsigned int length, char *filename);
Metadata *appendMetadata(Metadata *metadata, int offset, unsigned int length, char *filename);
Metadata *deleteMetadata(Metadata *metadata, char *filename);
void printFilenames(Metadata *metadata, int n);
unsigned int getNextFreeOffset(Metadata *metadata);
unsigned int getOffsetOfFile(Metadata *metadata, char *filename);
unsigned int getFileLength(Metadata *metadata, char *filename);

#endif //FILE_SYSTEM_METADATA_H
