//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#include "diskutilities.h"
#include "metadata.h"
#include "helper.h"
#include <stdlib.h>
#include <stdio.h>
#include <curses.h>

void listFiles(Metadata *metadata, int n) {
    printFilenames(metadata, n);
}

Metadata *formatDisk(Metadata *metadata) {
    if (metadata == NULL) {
        return NULL;
    }

    Metadata *currentMetadataRecord = metadata;
    while (currentMetadataRecord != NULL) {
        Metadata *temp = currentMetadataRecord;
        currentMetadataRecord = currentMetadataRecord->next;
        free(temp);
    }

    return NULL;
}

Metadata *deleteFile(Metadata *metadata, char *filename) {
    return deleteMetadata(metadata, filename);
}

Metadata *readTableFromDisk() {
    FILE *disk = fopen(DISK, "rb");

    if (disk == NULL) {
        return NULL;
    }

    fseek(disk, 0, SEEK_SET);
    unsigned int totalFiles;
    fread(&totalFiles, sizeof(unsigned int), 1, disk);

    if (totalFiles == 0) {
        return NULL;
    }

    Metadata *metadata = createMetadata(0, 0, "");
    fread(metadata, 24, 1, disk);
    Metadata *metadataHead = metadata;
    for (int i = 0; i < totalFiles - 1; i++) {
        metadata->next = createMetadata(0, 0, "");
        fread(metadata->next, 24, 1, disk);
        metadata = metadata->next;
    }

    fclose(disk);

    return metadataHead;
}

void writeTableToDisk(Metadata *metadata) {
    if (metadata == NULL) {
        return;
    }

    Metadata *currentMetadataRecord = metadata;
    FILE *disk = fopen(DISK, "rb+");

    if (disk == NULL) {
        return;
    }

    fseek(disk, 4, SEEK_SET);
    while (currentMetadataRecord != NULL) {
        fwrite(currentMetadataRecord, 24, 1, disk);
        currentMetadataRecord = currentMetadataRecord->next;
    }
    fclose(disk);
}

Metadata *importFile(Metadata *metadata, char *source, char *destination) {
    if (source == NULL || destination == NULL) {
        return metadata;
    }

    FILE *sourceFile = fopen(source, "rb");
    FILE *disk = fopen(DISK, "rb+");

    if (sourceFile == NULL || disk == NULL) {
        return metadata;
    }

    unsigned int offset = getNextFreeOffset(metadata);
    fseek(disk, offset, SEEK_SET);
    int fileLength = 0;

    char copyBuffer;
    while (fread(&copyBuffer, sizeof(char), 1, sourceFile) != 0) {
        fileLength++;
        if (fwrite(&copyBuffer, sizeof(char), 1, disk) == FALSE) {
            return metadata;
        }
    }
    metadata = appendMetadata(metadata, offset, fileLength, destination);

    fclose(disk);
    fclose(sourceFile);

    return metadata;
}

Metadata *exportFile(Metadata *metadata, char *source, char *destination) {
    if (source == NULL || destination == NULL) {
        return metadata;
    }

    FILE *disk = fopen(DISK, "rb");
    FILE *destinationFile = fopen(destination, "wb");

    if (disk == NULL || destinationFile == NULL) {
        return metadata;
    }

    char copyBuffer;
    int fileLength = 0;
    unsigned int offset = getOffsetOfFile(metadata, source);
    unsigned int fileLengthInDisk = getFileLength(metadata, source);
    fseek(disk, offset, SEEK_SET);

    while (fread(&copyBuffer, sizeof(char), 1, disk) != 0 && fileLength < fileLengthInDisk) {
        fileLength++;
        if (fwrite(&copyBuffer, sizeof(char), 1, destinationFile) == FALSE) {
            return metadata;
        }
    }

    fclose(destinationFile);
    fclose(disk);

    return metadata;
}

unsigned int readNFromDisk() {
    FILE *disk = fopen(DISK, "rb");
    if (disk == NULL) {
        return 0;
    }
    fseek(disk, 0, SEEK_SET);

    unsigned int n;
    fread(&n, sizeof(unsigned int), 1, disk);
    fclose(disk);
    return  n;
}

void writeNToDisk(unsigned int n) {
    FILE *disk = fopen(DISK, "r+");
    if (disk == NULL) {
        return;
    }

    fseek(disk, 0, SEEK_SET);
    fwrite(&n, sizeof(unsigned int), 1, disk);
    fclose(disk);
}