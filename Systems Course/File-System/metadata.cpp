//
// Created by Ajay Raj Nelapudi on 2019-05-30.
//

#include "metadata.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

Metadata *createMetadata(unsigned int offset, unsigned int length, char *filename) {
    Metadata *metadata = (Metadata *)malloc(sizeof(Metadata));
    metadata->offset = offset;
    metadata->length = length;
    strcpy(metadata->filename, filename);
    metadata->next = NULL;

    return metadata;
}

Metadata *appendMetadata(Metadata *metadata, int offset, unsigned int length, char *filename) {
    Metadata *newMetadata = createMetadata(offset, length, filename);

    if (metadata == NULL) {
        return newMetadata;
    }

    Metadata *currentMetadataRecord = metadata;
    while (currentMetadataRecord->next != NULL) {
        currentMetadataRecord = currentMetadataRecord->next;
    }
    currentMetadataRecord->next = newMetadata;

    return metadata;
}

Metadata *deleteMetadata(Metadata *metadata, char *filename) {
    if (metadata == NULL) {
        return NULL;
    }

    if (strcmp(metadata->filename, filename) == 0) {
        Metadata *temp = metadata;
        metadata = metadata->next;
        free(temp);
        return metadata;
    }

    Metadata *previousMetadataRecord = NULL;
    Metadata *currentMetadataRecord = metadata;

    while (currentMetadataRecord != NULL && strcmp(currentMetadataRecord->filename, filename) != 0) {
        previousMetadataRecord = currentMetadataRecord;
        currentMetadataRecord = currentMetadataRecord->next;
    }

    Metadata *temp = currentMetadataRecord;
    if (currentMetadataRecord != NULL && previousMetadataRecord != NULL) {
        previousMetadataRecord->next = currentMetadataRecord->next;
    } else if (previousMetadataRecord != NULL) {
        previousMetadataRecord->next = NULL;
    }
    free(temp);

    return metadata;
}

void printFilenames(Metadata *metadata, int n) {
    int fileCounter = 0;
    Metadata *currentMetadataRecord = metadata;
    while (currentMetadataRecord != NULL && fileCounter < n) {
        printf("%s\n", currentMetadataRecord->filename);
        currentMetadataRecord = currentMetadataRecord->next;
        fileCounter++;
    }
}

unsigned int getNextFreeOffset(Metadata *metadata) {
    if (metadata == NULL) {
        return 2401320;
    }

    unsigned int nextFreeOffset = 0;
    Metadata *currentMetadataRecord = metadata;
    while (currentMetadataRecord != NULL) {
        nextFreeOffset = currentMetadataRecord->offset + currentMetadataRecord->length;
        currentMetadataRecord = currentMetadataRecord->next;
    }

    return nextFreeOffset;
}

unsigned int getOffsetOfFile(Metadata *metadata, char *filename) {
    if (metadata == NULL || filename == NULL) {
        return 2401320;
    }

    unsigned int offset = 0;
    Metadata *currentMetaRecord = metadata;
    while (currentMetaRecord != NULL) {
        if (strcmp(filename, currentMetaRecord->filename) == 0) {
            offset = currentMetaRecord->offset;
        }
        currentMetaRecord = currentMetaRecord->next;
    }

    if (offset == 0) {
        offset = getNextFreeOffset(metadata);
    }

    return offset;
}

unsigned int getFileLength(Metadata *metadata, char *filename) {
    if (metadata == NULL || filename == NULL) {
        return 0;
    }

    unsigned int fileLength = 0;
    Metadata *currentMetaRecord = metadata;
    while (currentMetaRecord != NULL) {
        if (strcmp(filename, currentMetaRecord->filename) == 0) {
            fileLength = currentMetaRecord->length;
        }
        currentMetaRecord = currentMetaRecord->next;
    }

    return fileLength;
}