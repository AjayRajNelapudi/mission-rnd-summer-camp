#include <stdio.h>
#include "infixtopostfix.h"

int main() {
    char infix[] = "(a + b) * (c / d) + e";
    char postfix[10];
    infixToPostfix(infix, postfix, NULL);
    printf("%s", postfix);

    return 0;
}