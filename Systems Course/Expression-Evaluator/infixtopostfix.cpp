//
// Created by Ajay Raj Nelapudi on 2019-05-28.
//

#include "infixtopostfix.h"
#include "stack.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

int precedenceOf(char operatorSymbol) {
    switch(operatorSymbol) {
        case '^': return 1;
        case '/': return 2;
        case '%': return 2;
        case '*': return 2;
        case '+': return 3;
        case '-': return 3;
        case -1: return 4;
        default: return 4;
    }
}

int getValue(char *token, char **symbolTable) {

}

Stack* popAndAppendUntillBraces(Stack *top, char *postfix, int *postfixIndex) {
    int index = *postfixIndex;

    while (!isEmpty(top) && peek(top) != '(') {
        char operatorSymbol = peek(top);
        top = pop(top);
        postfix[index++] = operatorSymbol;
    }
    top = pop(top);

    *postfixIndex = index;
    return top;
}

Stack *popAndAppendAfterLowerPrecedenceOperator(Stack *top, char *postfix, int *postfixIndex, char *token) {
    int index = *postfixIndex;

    while (!isEmpty(top) && precedenceOf(peek(top)) < precedenceOf(token[0])) {
        char operatorSymbol = peek(top);
        top = pop(top);
        postfix[index++] = operatorSymbol;
    }
    top = push(top, token[0]);

    *postfixIndex = index;
    return top;
}

void joinWithSpaces(char *postfix, int length) {
    int l = length - 1;
    int i = length * 2 - 1;

    while (i >= 0) {
        postfix[i--] = postfix[l--];
        postfix[i--] = ' ';
    }
    postfix[length * 2] = '\0';
}

int infixToPostfix(char *infix, char *postfix, char **symbolTable) {
    if (infix == NULL || *infix == '\0') { // change to !isValidString(infix) later
        return -1;
    }

    if (postfix == NULL) {
        return -1;
    }

    Stack *top = NULL;
    int postfixIndex = 0;


    char *token = NULL;
    while ((token = strtok_r(infix, " ", &infix))) {
        if (token[0] == '(') {
            top = push(top, token[0]);
            if (token[1] != '\0') {
                token++;
                postfix[postfixIndex++] = getValue(token, symbolTable);
            }
        } else if (token[0] == ')') {
            top = popAndAppendUntillBraces(top, postfix, &postfixIndex);
        } else if (token[strlen(token) - 1] == ')' && token[0] != ')') {
            token[strlen(token) - 1] = '\0';
            postfix[postfixIndex++] = getValue(token, symbolTable);
            top = popAndAppendUntillBraces(top, postfix, &postfixIndex);
        } else if (precedenceOf(token[0]) < 4) {
            top = popAndAppendAfterLowerPrecedenceOperator(top, postfix, &postfixIndex, token);
        } else {
            postfix[postfixIndex++] = getValue(token, symbolTable);
        }
    }

    while (!isEmpty(top)) {
        postfix[postfixIndex++] = peek(top);
        top = pop(top);
    }
    postfix[postfixIndex] = '\0';

    joinWithSpaces(postfix, postfixIndex);
    return postfixIndex;
}