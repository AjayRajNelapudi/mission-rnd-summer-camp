//
// Created by Ajay Raj Nelapudi on 2019-05-28.
//

#ifndef EXPRESSION_EVALUATOR_STACK_H
#define EXPRESSION_EVALUATOR_STACK_H

typedef struct StackNode {
    int data;
    struct StackNode *next;
} Stack;

bool isEmpty(Stack *top);
Stack* makeNode(int data);
Stack* push(Stack *stack, int data);
int peek(Stack *top);
Stack* pop(Stack *stack);

#endif //EXPRESSION_EVALUATOR_STACK_H
