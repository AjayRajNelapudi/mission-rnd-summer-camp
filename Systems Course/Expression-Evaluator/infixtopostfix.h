//
// Created by Ajay Raj Nelapudi on 2019-05-28.
//

#ifndef EXPRESSION_EVALUATOR_INFIXTOPOSTFIX_H
#define EXPRESSION_EVALUATOR_INFIXTOPOSTFIX_H

int infixToPostfix(char *infix, char *postfix, char **symbolTable);

#endif //EXPRESSION_EVALUATOR_INFIXTOPOSTFIX_H
