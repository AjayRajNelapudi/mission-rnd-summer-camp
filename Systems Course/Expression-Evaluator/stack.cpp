//
// Created by Ajay Raj Nelapudi on 2019-05-28.
//

#include "stack.h"
#include <stdlib.h>

bool isEmpty(Stack *top) {
    if (top == NULL) {
        return true;
    }
    return false;
}

Stack* makeNode(int data) {
    Stack *newNode = (Stack *)malloc(sizeof(Stack));
    newNode->data = data;
    newNode->next = NULL;

    return newNode;
}

Stack* push(Stack *stack, int data) {
    Stack *newNode = makeNode(data);

    if (isEmpty(stack)) {
        return newNode;
    }

    newNode->next = stack;
    return newNode;
}

int peek(Stack *top) {
    if (isEmpty(top)) {
        return -1;
    }

    return top->data;
}

Stack* pop(Stack *stack) {
    if (isEmpty(stack)) {
        return NULL;
    }

    Stack *temp = stack;
    stack = stack->next;
    free(temp);
    return stack;
}