let StatesDistricts = {
    "Andhra Pradesh": [
        "Visakhapatnam",
        "Vijayawada",
        "Kakinada",
        "West Godavari"
    ],

    "Bihar": [
        "Patna",
        "Sasaram",
        "Gaya"
    ],

    "Kerala": [
        "Alleppy",
        "Ernakulam",
        "Palakkad"
    ],

    "Tamil Nadu": [
        "Nagapattinam",
        "Coimbatore",
        "Madurai"
    ],

    "Telangana": [
        "Hyderabad",
        "Khammam",
        "Karimnagar"
    ]
}

function setStates() {
    let states = Object.keys(StatesDistricts);
    let stateSelector = document.getElementById("states");
    states.forEach((state) => {
        let stateOption = document.createElement("option");
        stateOption.text = state;
        stateSelector.add(stateOption);
    })
}

function removeExistingOptions(selectbox) {
    for (var i = selectbox.options.length - 1; i >= 0; i--) {
        selectbox.remove(i);
    }
}

function setDistricts() {
    let state = document.getElementById("states").value;
    let districtSelector = document.getElementById("districts");
    removeExistingOptions(districtSelector);
    let districts = StatesDistricts[state];
    districts.forEach((district) => {
        let districtOption = document.createElement("option");
        districtOption.text = district;
        districtSelector.add(districtOption);
    })
}