import threading
import time
import sys

counter = 0

def worker(lock):
    global counter
    while True:
        counter += 1
        # time.sleep(0.2)

def watcher():
    global counter
    while True:
        print(counter)
        # time.sleep(0.2)

def main():
    lock = threading.Lock()
    worker1_thread = threading.Thread(target=worker, args=(lock))
    worker2_thread = threading.Thread(target=worker, args=(lock))
    watcher_thread = threading.Thread(target=watcher)

    worker1_thread.start()
    worker2_thread.start()
    # watcher_thread.start()

    worker1_thread.join()
    worker2_thread.join()
    # worker2_thread.join()

if __name__ == "__main__":
    main()