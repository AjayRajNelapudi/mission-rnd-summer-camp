import time
import threading

main_thread_id = threading.current_thread().ident

def activity(tag):

    for i in range(5):
        print("hello from", tag, "i =", i, "thread id:", main_thread_id)
        time.sleep(1)

def worker1():
    thread_id = threading.current_thread().ident
    print("worker1 thread id:", thread_id)
    activity("worker1")

def worker2():
    thread_id = threading.current_thread().ident
    print("worker2 thread id:", thread_id)
    activity("worker2")

def main():
    thread1 = threading.Thread(target=worker1)
    thread2 = threading.Thread(target=worker2)
    thread1.start()
    thread2.start()

if __name__ == "__main__":
    main()