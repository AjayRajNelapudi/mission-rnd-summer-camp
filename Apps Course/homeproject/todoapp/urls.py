from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from .views import *

urlpatterns = [
    path('list/', display_all_lists),
    path('list/<int:list_id>/', display_list_items, name='list'),
]