from django.conf import settings
from django.db import models

# Create your models here.
class ToDoList(models.Model):
    name = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

class ToDoItem(models.Model):
    description = models.CharField(max_length=300)
    due_date = models.DateField(null=True, blank=True)
    completed = models.BooleanField(default=False)

    todolist = models.ForeignKey(ToDoList, on_delete=models.CASCADE)

    def __str__(self):
        return self.description
