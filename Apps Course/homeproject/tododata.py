import os
import django
import openpyxl

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'homeproject.settings')
django.setup()

from todoapp.models import *

def populate_todolist(worksheet):
    for row in worksheet.iter_rows(values_only=True):
        try:
            todolist = ToDoList(name=row[0])
            todolist.save()
        except Exception as exp:
            print("Failed to insert todolist", row)
            print("Reason:", exp)

def populate_todoitem(worksheet):
    for row in worksheet.iter_rows(values_only=True):
        try:
            todolist = ToDoList.objects.get(name=row[3])
            todoitem = ToDoItem(description=row[0], due_date=row[1], todolist=todolist)
            todoitem.save()
        except Exception as exp:
            print("Failed to insert todoitem", row)
            print("Reason:", exp)

def main():
    tododata = openpyxl.load_workbook("data.xlsx")
    populate_todolist(tododata["list"])
    populate_todoitem(tododata["item"])
    tododata.close()

if __name__ == "__main__":
    main()