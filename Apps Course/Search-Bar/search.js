//wordsString.split("\n");

function setSearchBarText(text) {
    document.getElementById("search-bar").value = text;
}

let words = [
    "Ajay Raj",
    "Surya Teja",
    "Akhil",
    "Hema",
    "Bhargavi",
    "Abhishek",
    "Aditya",
    "Lokesh",
    "Ganapathi"
]

function matchWords(key) {
    return words.filter((word) => {
        return word.toLowerCase().startsWith(key.toLowerCase());
    });
}

function search() {
    let key = document.getElementById("search-bar").value;
    if (key.length == 0) {
        document.getElementById("search-items").innerHTML = "";
        return;
    }

    let matchedWords = matchWords(key);

    let datalist = document.getElementById("search-items");

    let result = "";
    matchedWords.forEach(matchedWord => {
        result += '<li> <input class="button is-white is-fullwidth" value=\'' + matchedWord + '\' onClick="setSearchBarText(\'' + matchedWord + '\')" /></li>';
        //result += '<button class="button is-white is-fullwidth" onClick="setSearchBarText(\'' + matchedWord + '\')" >' + matchedWord + '</button>';
    })
    datalist.innerHTML = result;
}