function renderCard(name, imageURL) {
    var cards = document.getElementById("cards");
    var cardHTML = '<div class="column">';
    cardHTML += '<div class="card-header-title">' + name + '</div>';
    cardHTML += '<figure class="image is-128x128">';
    cardHTML += '<img src="' + imageURL + '">';
    cardHTML += '</figure></div>';
    cards.innerHTML += cardHTML;
}

function searchSpotify(entry) {
    var token = 'BQD5gElvJlKd1sleCNi54LAJCkkICEp1LYoUn4L93mfLqJR4N4uPiWh-hvD-rvRheeNaM0eswaH-DrX7TaVn0v0nTZReeCGKkRe40Gch5v7r2FTN816gjQyFGp8vEVmN91LY3j8DibJXT_uMYzf31B9TnO3LksIwcw';

    if (entry == '') {
        document.getElementById("cards").innerHTML = "";
        return;
    }
    
    fetch('https://api.spotify.com/v1/search?q=' + entry + '&type=artist', {
        headers: {
            'Accept': "application/json",
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then(result => {
        const json = result.json();
        json.then(allData => {
            console.log(allData['artists']);
            var artistsData = allData['artists']['items'];
            artistsData.forEach(artist => {
                try {
                    var name = artist['name'];
                    var imageURL = artist['images'][0].url;

                    renderCard(name, imageURL);
                } catch (exp) {
                    console.log(name + ' has no image');
                }
            });
        });
    });
}

function search() {
    var entry = document.getElementById("search-bar").value;
    searchSpotify(entry);
}