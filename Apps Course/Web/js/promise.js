const sleep = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log("delayed");
        resolve("done");
    }, 2000);
    // reject('failed');
});

sleep
    .then(data => console.log(data))
    .catch(err => console.log('Error', err));