console.log('hi');

const promiseArray = [];
for (var i = 0; i < 100; i++) {
    console.log(i);

    const newPromise = new Promise((resolve, reject) => {
        resolve(i);
    });
    newPromise.then((number) => console.log('Done', number));
    promiseArray.push(newPromise);
}

Promise.all(promiseArray).then(() => console.log('finish'));
console.log('bye');