import os
import csv
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'IPL_App.settings')
django.setup()

from iplapp.models import *

def format_date(date):
    if "/" not in date:
        return date

    dd, mm, yyyy = date.split("/")
    return "%s-%s-%s" % ("20" + yyyy, mm, dd)

def populate_match(data_file):
    with open(data_file) as match_data:
        matches = csv.reader(match_data, delimiter=",")
        for match in matches:
            if match == []:
                continue

            try:
                match_obj = Match(
                    id=match[0],
                    season=match[1],
                    city=match[2],
                    date=format_date(match[3]),
                    team1=match[4],
                    team2=match[5],
                    toss_winner=match[6],
                    toss_decision=match[7],
                    result=match[8],
                    dl_applied=False if match[9] == "0" else True,
                    winner=match[10],
                    win_by_runs=match[11],
                    win_by_wickets=match[12],
                    player_of_match=match[13],
                    venue=match[14],
                    umpire1=match[15],
                    umpire2=match[16],
                    umpire3=match[17]
                )
                match_obj.save()
            except Exception as exp:
                print("Failed to insert:", match)
                print("Reason:", exp)

def populate_deliveries(data_file):
    with open(data_file) as deliveries_data:
        deliveries = csv.reader(deliveries_data, delimiter=",")
        for delivery in deliveries:
            if delivery == []:
                continue

            try:
                fk = Match.objects.get(id=delivery[0])
                delivery_obj = Deliveries(
                    match_id=fk,
                    inning=delivery[1],
                    batting_team=delivery[2],
                    bowling_team=delivery[3],
                    over=delivery[4],
                    ball=delivery[5],
                    batsman=delivery[6],
                    non_striker=delivery[7],
                    bowler=delivery[8],
                    is_super_over=False if delivery[9] == "0" else True,
                    wide_runs=delivery[10],
                    bye_runs=delivery[11],
                    legbye_runs=delivery[12],
                    noball_runs=delivery[13],
                    penalty_runs=delivery[14],
                    batsman_runs=delivery[15],
                    extra_runs=delivery[16],
                    total_runs=delivery[17],
                    player_dismissed=delivery[18],
                    dismissal_kind=delivery[19],
                    fielder=delivery[20]
                )
                delivery_obj.save()
            except Exception as exp:
                print("Failed to insert:", delivery)
                print("Reason:", exp)

if __name__ == "__main__":
    populate_match("matches.csv")
    populate_deliveries("deliveries.csv")