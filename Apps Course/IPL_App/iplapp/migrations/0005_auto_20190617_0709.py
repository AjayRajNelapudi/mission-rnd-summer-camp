# Generated by Django 2.2.1 on 2019-06-17 07:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('iplapp', '0004_auto_20190617_0608'),
    ]

    operations = [
        migrations.RenameField(
            model_name='deliveries',
            old_name='innings',
            new_name='inning',
        ),
    ]
