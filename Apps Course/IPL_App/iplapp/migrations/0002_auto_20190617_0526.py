# Generated by Django 2.2.1 on 2019-06-17 05:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('iplapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='umpire1',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='umpire1_ref', to='iplapp.Umpire'),
        ),
        migrations.AddField(
            model_name='match',
            name='umpire2',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='umpire2_ref', to='iplapp.Umpire'),
        ),
        migrations.AddField(
            model_name='match',
            name='umpire3',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='umpire3_refß', to='iplapp.Umpire'),
        ),
    ]
