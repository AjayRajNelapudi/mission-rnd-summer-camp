from django import forms
from django.urls import resolve
from django.shortcuts import render, redirect
from django.views import View
from iplapp.models import *
from django.core.paginator import Paginator
from django.db.models import Count

class Points(View):

    def get(self,request, *args, **kwargs):
        if "season" not in kwargs:
            kwargs["season"] = 2019

        team_points = Match.objects.values("winner").distinct().annotate(
                    points=Count("winner")*2
        ).filter(season=kwargs.get("season"))
        return render(
            request,
            template_name="points.html",
            context={
                "team_points": team_points,
                "seasons": range(2008, 2019),
            }
        )