from django import forms
from django.urls import resolve
from django.shortcuts import render, redirect
from django.views import View
from iplapp.models import *
from django.core.paginator import Paginator
from django.db.models import Count
from django.db.models import Q

class Team(View):

    def get(self, request, *args, **kwargs):
        if "season" not in kwargs:
            kwargs["season"] = 2019

        match_details = Match.objects.filter(
            Q(team1=kwargs.get("team")) | Q(team2=kwargs.get("team")),
            season=kwargs.get("season")
        ).values("team1", "team2", "winner", "win_by_runs", "win_by_wickets")
        return render(
            request,
            template_name="team.html",
            context={
                "match_details": match_details,
            }
        )
