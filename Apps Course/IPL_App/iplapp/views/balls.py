from django.shortcuts import render, redirect
from django.views import View
from iplapp.models import *
from django.contrib.auth.mixins import LoginRequiredMixin

class Balls(LoginRequiredMixin, View):
    login_url = "login"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            redirect(self.login_url)

        if not "innings" in kwargs:
            kwargs["innings"] = 1

        match = Match.objects.get(id=kwargs.get("match_id"))
        balls = Deliveries.objects.filter(match_id=kwargs.get("match_id"), inning=kwargs.get("innings"))
        return render(
            request,
            template_name="balls.html",
            context={
                "match_id": kwargs.get("match_id"),
                "match": match,
                "balls": balls,
            }
        )