from django import forms
from django.urls import resolve
from django.shortcuts import render, redirect
from django.views import View
from iplapp.models import *
from django.core.paginator import Paginator

class Matches(View):

    def get(self, request, *args, **kwargs):
        if kwargs:
            season = kwargs.get("season")
        else:
            season = 2018

        all_matches = Match.objects.filter(season=season)
        paginator = Paginator(all_matches, 8)

        seasons = range(2008, 2019)
        page = request.GET.get("page")
        matches = paginator.get_page(page)
        return render(
            request,
            template_name="matches.html",
            context={
                "matches": matches,
                "seasons": seasons,
            }
        )