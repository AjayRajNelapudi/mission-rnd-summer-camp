from django.views import View
from iplapp.forms import *
from django.shortcuts import redirect, render
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User

def logout_user(request):
    logout(request)
    return redirect("all_matches")

class Signup_Controller(View):

    def get(self, request, *args, **kwargs):
        signup_form = Signup_Form()
        return render (
            request,
            template_name="signup.html",
            context={
                'signup_form': signup_form
            }
        )

    def post(self, request, *args, **kwargs):

        user = User.objects.create_user(
            first_name=request.POST['firstname'],
            last_name=request.POST['lastname'],
            username=request.POST['username'],
            password=request.POST['password'],
        )

        if user is not None:
            login(request, user)
            return redirect("all_matches")
        else:
            return self.get(request, args, kwargs)

class Login_Controller(View):

    def get(self, request, *args, **kwargs):
        login_form = Login_Form()
        return render (
            request,
            template_name="login.html",
            context={
                'login_form': login_form
            }
        )

    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("all_matches")
        else:
            return self.get(request, args, kwargs)