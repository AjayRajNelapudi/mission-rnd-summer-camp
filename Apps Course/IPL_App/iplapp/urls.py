from django.conf import settings
from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path("signup/", Signup_Controller.as_view(), name="signup"),
    path("login/", Login_Controller.as_view(), name="login"),
    path("logout/", logout_user, name="logout"),

    path("matches/", Matches.as_view(), name="all_matches"),
    path("matches/<int:season>/", Matches.as_view(), name="season_matches"),
    path("match/<int:match_id>/innings/", Balls.as_view(), name="balls"),
    path("match/<int:match_id>/innings/<int:innings>/", Balls.as_view(), name="match_innings"),

    path("season/<int:season>/points/", Points.as_view(), name="points"),
]