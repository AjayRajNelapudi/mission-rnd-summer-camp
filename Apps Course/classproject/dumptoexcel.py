import os
import bs4
import click
import openpyxl
import requests
import html5lib
from contextlib import suppress


def prompt(message):
    response = input(message)
    if response in ("Y", "y"):
        return True
    return False


def get_table_data(table):
    if table == None:
        return []

    table_data = []
    row_counter = 0
    for row in table.find_all("tr"):
        excel_row = row.text.split()
        strip_front = len(str(row_counter))
        excel_row[0] = excel_row[0][strip_front:]
        table_data.append(excel_row)
        row_counter += 1
    table_data[0].pop(0)

    return table_data


def fill_sheet(results_ws, table_data):
    if results_ws == None or table_data == None:
        return

    for row in range(1, len(table_data) + 1):
        for column in range(1, len(table_data[row - 1]) + 1):
            results_ws.cell(row=row, column=column, value=table_data[row - 1][column - 1]) 


@click.command()
@click.argument("files", nargs=-1)
def dump_to_excel(files):
    url, destination = files

    raw_page = requests.get(url)
    soup = bs4.BeautifulSoup(raw_page.content, "html5lib")

    mocktest_results_wb = openpyxl.Workbook()
    mocktest_results_wb.title = "Mock Test Results"
    results_ws = mocktest_results_wb.active

    table_data = get_table_data(soup.find("table"))
    fill_sheet(results_ws, table_data)


    if os.path.isfile(destination) and prompt(destination + " file exists. Do you want to overwrite? [Y/N]"):
        mocktest_results_wb.save(destination)
    elif not os.path.isfile(destination):
        mocktest_results_wb.save(destination)

    

if __name__ == "__main__":
    dump_to_excel()
