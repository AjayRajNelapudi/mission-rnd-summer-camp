from rest_framework import routers, serializers, viewsets
from .views import *

class College_Serializer(serializers.ModelSerializer):
    class Meta:
        model = College
        fields = ("id", "name", "location", "acronym", "contact")

class MockTest1_Serializer(serializers.ModelSerializer):
    class Meta:
        model = MockTest1
        fields = ("id", "problem1", "problem2", "problem3", "problem4")

class Student_View_Serializer(serializers.ModelSerializer):
    total = MockTest1_Serializer(source="total", many=True)

    class Meta:
        model = Student
        fields = ("name", "email", "total")

class Student_MockTest_Serializer(serializers.ModelSerializer):
    mocktest1 = MockTest1_Serializer(many=False)

    class Meta:
        model = Student
        fields = ("id", "name", "dob", "email", "db_folder", "dropped_out", "mocktest1")

    def create(self, validated_data):
        mocktest1_data = validated_data.pop("mocktest1")
        college_obj = get_object_or_404(College, id=self.context['college_id'])
        student = Student.objects.create(**validated_data, college=college_obj)
        total = mocktest1_data['problem1'] + mocktest1_data['problem2'] + mocktest1_data['problem3'] + mocktest1_data['problem4']
        mocktest = MockTest1.objects.create(**mocktest1_data, student=student, total=total)
        return student

    def update(self, instance, validated_data):
        mocktest1_data = validated_data.pop("mocktest1")
        mocktest1 = MockTest1.objects.get(student=instance.id)

        instance.name = validated_data.get("name", instance.name)
        instance.dob = validated_data.get("dob", instance.dob)
        instance.email = validated_data.get("email", instance.email)
        instance.db_folder = validated_data.get("db_folder", instance.db_folder)
        instance.dropped_out = validated_data.get("dropped_out", instance.dropped_out)

        mocktest1.problem1 = mocktest1_data.get("problem1", mocktest1.problem1)
        mocktest1.problem2 = mocktest1_data.get("problem2", mocktest1.problem2)
        mocktest1.problem3 = mocktest1_data.get("problem3", mocktest1.problem3)
        mocktest1.problem4 = mocktest1_data.get("problem4", mocktest1.problem4)
        mocktest1.total = mocktest1.problem1 + mocktest1.problem2 + mocktest1.problem3 + mocktest1.problem4
        mocktest1.save()

        return instance