from onlineapp.serializers import *
from rest_framework import status
from onlineapp.models import *
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from rest_framework.authentication import *
from rest_framework.permissions import *


class CRUD_College_RestAPI(APIView):
    # authentication_classes = (BaseAuthentication, SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        college = College.objects.all()
        serializer = College_Serializer(college, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = College_Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

