from django.urls import resolve
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View
from onlineapp.forms import *
from django.contrib.auth.mixins import LoginRequiredMixin


class CRUD_Student_MockTest(LoginRequiredMixin, View):
    login_url = "http://localhost:8000/login/"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("login_user")

        if resolve(request.path_info).url_name == 'delete_student_mocktest' and kwargs:
            MockTest1.objects.get(student__id=kwargs['student_id']).delete()
            Student.objects.get(id=kwargs['student_id']).delete()
            return redirect('http://localhost:8000/college/' + str(kwargs['college_id']))

        student_form = Add_Student()
        mocktest1_form = Add_MockTest1()
        if 'student_id' in kwargs:
            student = Student.objects.get(id=kwargs['student_id'])
            student_form = Add_Student(instance=student)

            mocktest1 = MockTest1.objects.get(student__id=kwargs['student_id'])
            mocktest1_form = Add_MockTest1(instance=mocktest1)
            college_name = College.objects.get(id=kwargs['college_id'])

        return render (
            request,
            template_name="add_student.html",
            context={
                'student': student_form,
                'mocktest1': mocktest1_form,
                'collegeId': kwargs['college_id'],
                'collegeName': college_name,
            }
        )


    def post(self, request, *args, **kwargs):
        if resolve(request.path_info).url_name == 'edit_student_mocktest' and kwargs:
            student = Student.objects.get(id=kwargs['student_id'])
            student_form = Add_Student(request.POST, instance=student)
            mocktest1 = MockTest1.objects.get(student__id=kwargs['student_id'])
            mocktest1_form = Add_MockTest1(request.POST, instance=mocktest1)
            if student_form.is_valid() and mocktest1_form.is_valid():
                student_form.save()
                mocktest1_form.save()
            return redirect("http://localhost:8000/college/" + str(kwargs['college_id']) + "/")


        college_obj = get_object_or_404(College, id=kwargs['college_id'])
        student_form = Add_Student(request.POST)
        mocktest1_form = Add_MockTest1(request.POST)

        if student_form.is_valid():
            student = student_form.save(commit=False)
            student.college = college_obj
            student.save()


            if mocktest1_form.is_valid():
                mocktest1 = mocktest1_form.save(commit=False)
                total_marks = 0
                for i in range(1, 5):
                    total_marks += int(request.POST['problem' + str(i)])
                mocktest1.student = student
                mocktest1.total = total_marks
                mocktest1.save()

                return redirect("http://localhost:8000/college/" + str(kwargs['college_id']) + "/")

        else:
            student_form = Add_Student()
            mocktest1_form = Add_MockTest1()

            return render(
                request,
                template_name="add_student.html",
                context={
                    'student': student_form,
                    'mocktest1': mocktest1_form,
                }
            )



        student_form = Add_Student()
        mocktest1_form = Add_MockTest1()

        return render(
            request,
            template_name="add_student.html",
            context={
                'student': student_form,
                'mocktest1': mocktest1_form,
            }
        )

