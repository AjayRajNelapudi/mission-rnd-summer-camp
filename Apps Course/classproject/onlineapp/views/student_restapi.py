from onlineapp.serializers import *
from rest_framework import status
from onlineapp.models import *
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from rest_framework.authentication import *
from rest_framework.permissions import *


class CRUD_Student_RestAPI(APIView):
    # authentication_classes = (BaseAuthentication, SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        students = MockTest1.objects.filter(student__college__id=kwargs['college_id']).values_list("student__name", "student__email", "total")
        serializer = Student_View_Serializer(students, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = Student_MockTest_Serializer(data=request.data, context={'college_id': kwargs['college_id']})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        student = get_object_or_404(Student, id=pk)
        serializer = Student_MockTest_Serializer(student, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        student = get_object_or_404(Student, id=pk)
        student.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)