from django import forms
from django.urls import resolve
from django.shortcuts import render, redirect
from django.views import View
from onlineapp.models import College, Student, MockTest1
from onlineapp.forms import *
from django.contrib.auth.mixins import LoginRequiredMixin

class College_View(LoginRequiredMixin, View):
    login_url = "http://localhost:8000/login/"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("login_controller")

        if kwargs:
            if 'pk' in kwargs.keys():
                students = MockTest1.objects.filter(student__college__id=kwargs['pk']).values_list(
                    "student__name", "student__email", "total", "student__college__id", "student__id"
                )
            elif 'acronym' in kwargs.keys():
                students = MockTest1.objects.filter(student__college__acronym=kwargs['acronym']).values_list(
                    "student__name", "student__email", "total", "student__college__id", "student__id"
                )

            return render (
                request,
                template_name="students.html",
                context={
                    "students": students,
                    "collegeId": kwargs['pk'],
                }
            )

        colleges = College.objects.all()
        return render (
            request,
            template_name="colleges.html",
            context={
                "colleges": colleges,
            }
        )

class CRUD_College(LoginRequiredMixin, View):
    login_url = "http://localhost:8000/login/"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect(self.login_url)

        if resolve(request.path_info).url_name == 'delete_college' and kwargs:
            College.objects.get(pk=kwargs.get('pk')).delete()
            return redirect("colleges")

        form = Add_College()

        if kwargs:
            college = College.objects.get(**kwargs)
            form = Add_College(instance=college)

        return render (
            request,
            template_name="add_college.html",
            context={
                'form': form,
            }
        )

    def post(self, request, *args, **kwargs):
        if resolve(request.path_info).url_name == 'edit_college':
            college = College.objects.get(pk=kwargs.get('pk'))
            form = Add_College(request.POST, instance=college)
            if form.is_valid():
                form.save()
                return redirect("colleges")

        form = Add_College(request.POST)
        if form.is_valid():
            form.save()
            return redirect('colleges')

        return render (
            request,
            template_name="add_college.html",
            context={
                'form':form
            }
        )