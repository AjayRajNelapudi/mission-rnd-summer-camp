from django.conf import settings
from django.contrib import admin
from django.urls import path, re_path
from .views import *

urlpatterns = [
    # path('hello/', hello),
    # path('sample/', sample),
    # path('get_my_college/', get_my_college),
    # path('get_all_colleges/', get_all_colleges),
    # path('college_student_info/<int:college_id>/', get_college_student_info, name='college_student_info')
    path('', homepage, name="home"),

    # path(r'testpath/', test, name="test"),

    path(r"api/v1/colleges/", CRUD_College_RestAPI.as_view(), name="get_colleges"),
    # path("api/v1/college/<int:college_id>/student/<int:student_id>", Add_Student.as_view(), name="add_student"),
    path(r"api/v1/college/<int:college_id>/student/", CRUD_Student_RestAPI.as_view(), name="add_student"),
    path(r"api/v1/student/<int:pk>/", CRUD_Student_RestAPI.as_view(), name="edit_student"),

    path(r'college/', College_View.as_view(), name="colleges"),
    path(r'college/<int:pk>/', College_View.as_view(), name="college_students"),
    # path(r'college/<str:acronym>/', College_View.as_view(), name="college_details"),

    path(r'college/add/', CRUD_College.as_view(), name="add_college"),
    path(r'college/<int:pk>/edit', CRUD_College.as_view(), name="edit_college"),
    path(r'college/<int:pk>/delete', CRUD_College.as_view(), name="delete_college"),

    path(r'college/<int:college_id>/add/', CRUD_Student_MockTest.as_view(), name="add_student_mocktest"),
    path(r'college/<int:college_id>/student/<int:student_id>/edit/', CRUD_Student_MockTest.as_view(), name="edit_student_mocktest"),
    path(r'college/<int:college_id>/student/<int:student_id>/delete/', CRUD_Student_MockTest.as_view(), name="delete_student_mocktest"),

    path(r'login/', Login_Controller.as_view(), name="login_user"),
    path(r'signup/', SignUp_Controller.as_view(), name="signup_user"),
    path(r'logout/', logout_user, name="logout_user")
]
