from django import forms


class SignUp(forms.Form):
    firstname = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'input',
            'placeholder': 'firstname'
        })
    )

    lastname = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'input',
            'placeholder': 'lastname'
        })
    )

    username = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'input',
            'placeholder': 'username'
        })
    )

    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={
            'class': 'input',
            'placeholder': 'password'
        })
    )