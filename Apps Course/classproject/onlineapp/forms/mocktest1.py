from django import forms
from onlineapp.models import MockTest1

class Add_MockTest1(forms.ModelForm):
    class Meta:
        model = MockTest1
        exclude = ['id', 'total', 'student']
        fields = ['problem1', 'problem2', 'problem3', 'problem4']
        widgets = {
            'problem1': forms.NumberInput(),
            'problem2': forms.NumberInput(),
            'problem3': forms.NumberInput(),
            'problem4': forms.NumberInput(),
        }