from django import forms
from onlineapp.models import College

class Add_College(forms.ModelForm):
    class Meta:
        model = College
        exclude = ['id']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter college name'}),
            'location': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter college location'}),
            'acronym': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter college acronym'}),
            'contact': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter college contact'}),
        }