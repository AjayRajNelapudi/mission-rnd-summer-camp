from django import forms
from onlineapp.models import Student

class Add_Student(forms.ModelForm):
    class Meta:
        model = Student
        exclude = ['id', 'dob', 'college']
        fields = ['name', 'email', 'db_folder', 'dropped_out']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter name'}),
            'email': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter email'}),
            'db_folder': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter folder name'}),
            'dropped_out': forms.CheckboxInput(),
        }