import os
import django
import openpyxl
import dumptoexcel

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'classproject.settings')
django.setup()

from onlineapp.models import *

def populate_colleges(worksheet):
    skip_flag = True
    for row in worksheet.iter_rows(values_only=True):
        if skip_flag:
            skip_flag = False
            continue

        try:
            college = College(name=row[0], acronym=row[1], location=row[2], contact=row[3])
            college.save()
        except:
            print("Failed to insert college tuple:", row)

def populate_students(worksheet, dropped_out=False):
    for row in worksheet.iter_rows(values_only=True, max_col=4):
        try:
            college_data = College.objects.get(acronym=row[1])
             # db_folder_name = "ol2016_%s_%s_mock" % (row[1], row[-1].lower())
            student = Student(name=row[0], college=college_data, email=row[2], db_folder=row[-1].lower(), dropped_out=dropped_out)
            student.save()
        except Exception as exp:
            print("Failed to insert student tuple:", row)
            print("Reason:", exp)

def populate_marks(worksheet):
    skip_flag = True
    for row in worksheet.iter_rows(values_only=True):
        if skip_flag:
            skip_flag = False
            continue

        try:
            student_data = Student.objects.get(db_folder=row[0].split("_")[2])
            mocktest1 = MockTest1(student=student_data, problem1=row[1], problem2=row[2], problem3=row[3],
                                  problem4=row[4], total=row[5])
            mocktest1.save()
        except:
            print("Failed to insert marks tuple:", row)

def main():
    workbook = openpyxl.load_workbook("/Users/ajayraj/Documents/MRND-Summer-Python/classproject/students.xlsx")
    populate_colleges(workbook["Colleges"])
    populate_students(workbook["Current"])
    populate_students(workbook["Deletions"], dropped_out=True)
    workbook.close()

    # files = ("https://d1b10bmlvqabco.cloudfront.net/attach/inpg92dp42z2zo/hdff4poirlh7i6/io5hun2sdr21/mock_results.html", "/Users/ajayraj/Documents/MRND-Summer-Python/classproject/marks.xlsx")
    # try:
    #     dumptoexcel.dump_to_excel(files)
    # except:
    #     pass

    workbook = openpyxl.load_workbook("/Users/ajayraj/Documents/MRND-Summer-Python/classproject/marks.xlsx")
    populate_marks(workbook.active)
    workbook.close()

if __name__ == "__main__":
    main()